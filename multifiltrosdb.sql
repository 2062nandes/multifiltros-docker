## Redaxo Database Dump Version 5
## Prefix rex_
## charset utf-8

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `rex_action`;
CREATE TABLE `rex_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `preview` text,
  `presave` text,
  `postsave` text,
  `previewmode` tinyint(4) DEFAULT NULL,
  `presavemode` tinyint(4) DEFAULT NULL,
  `postsavemode` tinyint(4) DEFAULT NULL,
  `createuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `updatedate` datetime NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_article`;
CREATE TABLE `rex_article` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `catpriority` int(10) unsigned NOT NULL,
  `startarticle` tinyint(1) NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `path` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `clang_id` int(10) unsigned NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  `yrewrite_url` varchar(255) NOT NULL,
  `yrewrite_canonical_url` varchar(255) NOT NULL,
  `yrewrite_priority` varchar(5) NOT NULL,
  `yrewrite_changefreq` varchar(10) NOT NULL,
  `yrewrite_title` varchar(255) NOT NULL,
  `yrewrite_description` text NOT NULL,
  `yrewrite_index` tinyint(1) NOT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `find_articles` (`id`,`clang_id`),
  KEY `id` (`id`),
  KEY `clang_id` (`clang_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_article` WRITE;
/*!40000 ALTER TABLE `rex_article` DISABLE KEYS */;
INSERT INTO `rex_article` VALUES 
  (1,1,0,'Inicio','Inicio',1,1,1,'|',1,'2018-02-26 04:50:44','2018-08-09 16:01:33',1,1,'admin','admin',0,'','','','','','',0),
  (2,2,0,'Nosotros','Nosotros',2,0,1,'|',1,'2018-02-28 15:08:22','2018-08-25 11:25:30',6,1,'admin','soporte-multifiltros',0,'','','','','','',0),
  (3,3,0,'Contactos','',0,0,2,'|',1,'2018-02-28 15:09:39','2018-08-28 17:12:30',6,1,'admin','admin',0,'','','','','','',0),
  (4,4,0,'Tecfil','Tecfil',2,0,3,'|',1,'2018-02-28 15:10:45','2018-08-23 16:54:01',6,1,'admin','admin',0,'','','','','','',0),
  (5,5,0,'Unifilter','',0,0,4,'|',1,'2018-02-28 15:10:56','2018-08-23 17:38:24',6,1,'admin','admin',0,'','','','','','',0),
  (6,6,0,'Parker - Racor','',0,0,5,'|',1,'2018-02-28 15:11:02','2018-08-23 17:45:29',6,1,'admin','admin',0,'','','','','','',0),
  (7,7,0,'Keltec','',0,0,6,'|',1,'2018-02-28 15:11:12','2018-08-24 10:24:07',6,1,'admin','admin',0,'','','','','','',0),
  (8,8,0,'Baldwin','',0,0,7,'|',1,'2018-02-28 15:11:23','2018-08-24 10:28:34',6,1,'admin','admin',0,'','','','','','',0),
  (9,9,0,'Mc repuestos','',0,0,8,'|',1,'2018-02-28 15:11:33','2018-08-24 12:29:27',6,1,'admin','admin',0,'','','','','','',0),
  (10,10,0,'Rama','',0,0,9,'|',1,'2018-02-28 15:11:39','2018-08-28 16:37:08',6,1,'admin','admin',0,'','','','','','',0),
  (12,12,0,'Accesorios','',0,0,10,'|',1,'2018-02-28 15:11:57','2018-08-25 11:15:55',6,1,'admin','soporte-multifiltros',0,'','','','','','',0),
  (13,13,1,'_Menú Vertical','Inicio',1,0,4,'|1|',0,'2018-02-28 15:16:49','2018-03-28 12:07:05',7,1,'admin','admin',0,'','','','','','',0),
  (14,14,1,'Slider','Inicio',0,0,5,'|1|',0,'2018-02-28 16:11:54','2018-08-27 09:40:58',7,1,'admin','admin',0,'','','','','','',0),
  (15,15,1,'_Pie','Inicio',0,0,7,'|1|',0,'2018-02-28 22:49:08','2018-08-13 10:41:20',7,1,'admin','admin',0,'','','','','','',0),
  (16,16,1,'_Cabecera','Inicio',0,0,2,'|1|',0,'2018-02-28 22:51:03','2018-08-09 16:59:01',7,1,'admin','admin',0,'','','','','','',0),
  (17,17,1,'_Redes sociales','Inicio',0,0,8,'|1|',0,'2018-03-02 12:06:42','2018-08-13 10:32:53',7,1,'admin','admin',0,'','','','','','',0),
  (18,18,1,'_Menu Inicial','Inicio',0,0,3,'|1|',0,'2018-03-10 09:35:08','2018-03-10 09:35:50',7,1,'admin','admin',0,'','','','','','',0),
  (20,19,1,'Error 404','Inicio',0,0,9,'|1|',1,'2018-03-17 10:55:23','2018-03-19 16:31:15',7,1,'admin','admin-multifiltros',0,'','','','','','',0),
  (21,20,0,'Tb Fil','',0,0,11,'|',1,'2018-03-28 12:05:52','2018-08-24 11:12:34',6,1,'admin','admin',0,'','','','','','',0),
  (22,21,1,'Poppup','Inicio',0,0,10,'|1|',0,'2018-08-09 16:02:01','2018-08-13 12:16:50',7,1,'admin','admin',0,'','','','','','',0),
  (23,22,1,'Carousel de Marcas','Inicio',0,0,6,'|1|',0,'2018-08-24 12:36:03','2018-08-27 10:05:43',7,1,'admin','admin',0,'','','','','','',0);
/*!40000 ALTER TABLE `rex_article` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_article_slice`;
CREATE TABLE `rex_article_slice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clang_id` int(10) unsigned NOT NULL,
  `ctype_id` int(10) unsigned NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `value1` text,
  `value2` text,
  `value3` text,
  `value4` text,
  `value5` text,
  `value6` text,
  `value7` text,
  `value8` text,
  `value9` text,
  `value10` text,
  `value11` text,
  `value12` text,
  `value13` text,
  `value14` text,
  `value15` text,
  `value16` text,
  `value17` text,
  `value18` text,
  `value19` text,
  `value20` text,
  `media1` varchar(255) DEFAULT NULL,
  `media2` varchar(255) DEFAULT NULL,
  `media3` varchar(255) DEFAULT NULL,
  `media4` varchar(255) DEFAULT NULL,
  `media5` varchar(255) DEFAULT NULL,
  `media6` varchar(255) DEFAULT NULL,
  `media7` varchar(255) DEFAULT NULL,
  `media8` varchar(255) DEFAULT NULL,
  `media9` varchar(255) DEFAULT NULL,
  `media10` varchar(255) DEFAULT NULL,
  `medialist1` text,
  `medialist2` text,
  `medialist3` text,
  `medialist4` text,
  `medialist5` text,
  `medialist6` text,
  `medialist7` text,
  `medialist8` text,
  `medialist9` text,
  `medialist10` text,
  `link1` varchar(10) DEFAULT NULL,
  `link2` varchar(10) DEFAULT NULL,
  `link3` varchar(10) DEFAULT NULL,
  `link4` varchar(10) DEFAULT NULL,
  `link5` varchar(10) DEFAULT NULL,
  `link6` varchar(10) DEFAULT NULL,
  `link7` varchar(10) DEFAULT NULL,
  `link8` varchar(10) DEFAULT NULL,
  `link9` varchar(10) DEFAULT NULL,
  `link10` varchar(10) DEFAULT NULL,
  `linklist1` text,
  `linklist2` text,
  `linklist3` text,
  `linklist4` text,
  `linklist5` text,
  `linklist6` text,
  `linklist7` text,
  `linklist8` text,
  `linklist9` text,
  `linklist10` text,
  `article_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `revision` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slice_priority` (`article_id`,`priority`,`module_id`),
  KEY `clang_id` (`clang_id`),
  KEY `article_id` (`article_id`),
  KEY `find_slices` (`clang_id`,`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_article_slice` WRITE;
/*!40000 ALTER TABLE `rex_article_slice` DISABLE KEYS */;
INSERT INTO `rex_article_slice` VALUES 
  (2,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','4,5,6,7,8,9,10,20,12','','','','','','','','','',13,1,'2018-02-28 15:57:16','2018-03-28 12:07:05','admin','admin',0),
  (3,1,1,2,'<p><a href=\"http://www.tecfil.com.br/\" target=\"_blank\">Tecfil</a>&nbsp;se destaca por su agilidad en el lanzamiento de nuevos productos. Es en el Departamento de Nuevos Productos que comienza el proceso de crear un filtro en particular. Se llevan a cabo estudios de mercado, algunas líneas de investigación y maquinaria apropiada antes de lanzar cualquier producto.<br></p>\r\n<p>El objetivo es siempre para asegurar la máxima cobertura de la flota, ahora alrededor del 95% contestado con productos&nbsp;<a href=\"http://www.tecfil.com.br/\" target=\"_blank\">Tecfil</a>. Con el lanzamiento constante de nuevos modelos de varios fabricantes de automóviles,&nbsp;<a href=\"http://www.tecfil.com.br/\" target=\"_blank\">Tecfil</a>&nbsp;siempre debe estar en sintonía con el mercado y asegurarse de que el filtro de llegar lo más rápido posible a los canales de venta.\r\n</p>\r\n<p>Todos los departamentos están involucrados: ingeniería, calidad, asistencia técnica, adquisición, producción, logística, marketing y ventas. Cada uno con su propia función para liderar el mercado filtros de calidad y con la garantía de&nbsp;<a href=\"http://www.tecfil.com.br/\" target=\"_blank\">Tecfil</a>.</p>\r\n<p><strong>Comprender el filtro:&nbsp;</strong>El filtro es una parte integral del motor cuyo propósito principal es para retener las partículas nocivas para el sistema que puede ser llevado por el aire, aceite y combustible.</p>\r\n<p>La retención se produce por medio de un proceso físico o mecánico mediada por un medio de filtro de pantalla, algodón, lana, poliéster, fieltro, fibra, papel, etc. Por lo tanto mejores son los filtros, más que evitar el desgaste prematuro del motor.</p>\r\n<p>Los filtros Tecfil se producen como fabricantes de procesos únicos, siguiendo todos los requisitos del mercado. Más de 60 años de tradición, evolución y mejora.</p>\r\n<p>Todos nuestros productos son controlados y constantemente probados en nuestro laboratorio, así como por varios fabricantes de automóviles.</p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,2,'2018-02-28 17:03:45','2018-03-20 10:26:45','admin','admin',0),
  (13,1,1,3,'Tipos de filtros','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,4,'2018-02-28 17:16:27','2018-03-20 10:34:15','admin','admin',0),
  (15,1,1,4,'<p>Este filtro es una parte vital en el proceso de filtración o el bloqueo de las partículas no deseadas del aire para el motor.&nbsp;Su característica principal es retener las impurezas del ambiente exterior dejando el motor en condiciones ideales para el perfecto estado de funcionamiento.&nbsp;Reemplazar el filtro de aire en el momento adecuado también evita el consumo excesivo de combustible, el calentamiento del motor, la pérdida de potencia y el aumento de los gases de efecto invernadero a través de los gases de escape.</p>','','','','','','','','','','','','','','','','','','','','01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-02-28 17:57:25','2018-03-20 10:34:56','admin','admin',0),
  (16,1,1,5,'<p>Bien conocido en el mercado de la automoción filtración, filtro de aceite del motor es un elemento muy importante para el perfecto funcionamiento del vehículo.&nbsp;Su función principal es prevenir el paso de partículas no deseadas (a partir de las piezas de fricción) en el motor evitando así un desgaste excesivo de los componentes internos durante el funcionamiento.</p>','','','','','','','','','','','','','','','','','','','','02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-02-28 20:28:42','2018-02-28 20:28:49','admin','admin',0),
  (18,1,1,1,'<p>Somos distribuidores a nivel nacional de filtros, lubricantes, refrigerantes, siliconas,bombas de lubricación y productos afines al sector automotriz.</p>\r\n<p>Contamos con talento humano altamente calificado y atención especializada, con los mejores precios.</p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',1,2,'2018-02-28 21:20:19','2018-03-22 11:15:33','admin','admin',0),
  (20,1,1,2,'<p>Una vez generado por el compresor de aire, el aire (comprimido) a menudo debe filtrarse aún más para cumplir con las necesidades exactas de las aplicaciones industriales para las que se utiliza.<br></p>\r\n<p>Esto se hace más eficazmente mediante el uso de un filtro coalescente aguas abajo o una serie de filtros.&nbsp;Es un concepto erróneo común que los compresores de aire \"sin aceite\" no requieren esta precaución.<br>Sin embargo, el aire atmosférico típicamente contiene una cantidad significativa de agua, vapor de aceite y otros contaminantes, especialmente en áreas industriales.<br>Luego de la compresión, estos contaminantes se concentran en el aire comprimido, ya sea que la máquina esté \"libre de aceite\" o se haya inundado con aceite.<br>En consecuencia, el uso de filtros finos y coalescentes es esencial para ambos tipos de compresores, a fin de evitar la acumulación de dichos contaminantes en la maquinaria neumática.&nbsp;<br>Los filtros coalescentes <strong>KELTEC</strong> son la forma más sencilla de evitar dicha contaminación en sus costosos equipos.&nbsp;Nuestros filtros proporcionan el nivel más alto de aire comprimido limpio con una pérdida de energía mínima (caída de presión).<br>Mediante la selección del grado apropiado de medios de filtración de microvidrio de borosilicato, y manteniendo la cantidad, el diámetro y la dirección de las fibras individuales, los filtros coalescentes <strong>KELTEC</strong> garantizan que su sistema de compresor de aire funcionará correctamente con un mínimo mantenimiento operativo.</p>','','','','','','','','','','','','','','','','','','','','keltec01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',7,5,'2018-02-28 21:44:00','2018-03-29 12:25:53','admin','admin',0),
  (21,1,1,3,'<p><u></u>Lo mejor en filtración de aire comprimido Cuando se trata de mantener el aire comprimido limpio y libre de aceite, nadie se ajusta a la ley como <strong>KELTEC</strong> Technolab.&nbsp;La línea avanzada de filtros coalescentes de <strong>KELTEC</strong> Technolab para sistemas de aire comprimido ofrece una combinación excepcional de rendimiento excepcional, fiabilidad comprobada y valor poco común.</p>\r\n<p>La línea de productos de filtro de aire comprimido <strong>KELTEC</strong> Technolab cubre una amplia gama de productos para manejar prácticamente cualquier caudal y condición de funcionamiento.&nbsp;Nuestra gama de coalescedores de alta eficiencia, coalescedores de partículas / de propósito general y filtros de carbón adsorbente ofrece soluciones para prácticamente todas las aplicaciones de filtración de aire comprimido.</p>','','','','','','','','','','','','','','','','','','','','keltec03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',7,5,'2018-02-28 21:53:50','2018-03-29 12:27:04','admin','admin',0),
  (22,1,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',3,6,'2018-02-28 22:54:05','2018-03-20 17:00:10','admin','admin-multifiltros',0),
  (27,1,1,1,'VENTA Y DISTRIBUCIÓN','DE FILTROS','PARA VEHÍCULOS','','','','','','','','','','','','','','','','','','multifiltros-logo.png','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','1,2,3','','','','','','','','','',16,7,'2018-02-28 23:00:41','2018-08-09 16:59:01','admin','admin',0),
  (28,1,1,1,'<p>La calidad que es líder</p>','','','','','','','','','','','','','','','','','','','','tecfil.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,3,'2018-03-01 15:06:21','2018-08-23 16:54:01','admin','admin',0),
  (29,1,1,1,'<p>Nos destacamos en un mercado cada vez más competitivo, por la calidad de sus productos y por la competencia y confiabilidad de sus profesionales.</p>','','','','','','','','','','','','','','','','','','','','unifilter.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,3,'2018-03-01 15:07:33','2018-08-23 17:38:24','admin','admin',0),
  (30,1,1,1,'<p>Filtros de Racor</p>','','','','','','','','','','','','','','','','','','','','parker-racor.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,3,'2018-03-01 15:09:16','2018-08-23 17:45:29','admin','admin',0),
  (31,1,1,1,'<p>Filtros separadores de compresores</p>','','','','','','','','','','','','','','','','','','','','keltec_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',7,3,'2018-03-01 15:09:35','2018-08-24 10:24:07','admin','admin',0),
  (32,1,1,1,'<p>Filtros</p>','','','','','','','','','','','','','','','','','','','','baldwin_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',8,3,'2018-03-01 15:10:28','2018-08-24 10:28:34','admin','admin',0),
  (33,1,1,1,'<p>Empresa dedicada a la fabricación de repuestos automotor y en especial a la línea de conjuntos para el filtrado de combustibles.</p>','','','','','','','','','','','','','','','','','','','','mc-repuestos.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',9,3,'2018-03-01 15:10:39','2018-08-24 12:29:27','admin','admin',0),
  (37,1,1,1,'','','','','','','','','','','','','','','','','','','','','nosotros.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',2,3,'2018-03-01 15:51:55','2018-08-25 11:25:30','admin','soporte-multifiltros',0),
  (38,1,1,2,'','','','','','','','','','','','','','','','','','','','','slide.jpg','','','','','','','','','','','','','','','','','','','','5','','','','','','','','','','','','','','','','','','','',14,8,'2018-03-01 15:59:26','2018-03-23 12:33:22','admin','admin',0),
  (39,1,1,1,'','','','','','','','','','','','','','','','','','','','','banner-65a_os.jpg','','','','','','','','','','','','','','','','','','','','4','','','','','','','','','','','','','','','','','','','',14,8,'2018-03-01 15:59:32','2018-03-31 12:28:53','admin','admin',0),
  (40,1,1,2,'<p>Cuando los motores demandan separación de agua de alta capacidad y filtración de combustible de servicio pesado, la Serie Turbine es la protección de motor más completa, eficiente y confiable que puede instalar.&nbsp;</p>\r\n<p>Simbolizando el continuo compromiso de Racor con la ciencia de la filtración, la Serie Turbine ha establecido su posición como el filtro / separador a menudo imitado, pero nunca igualado.<br>Los modelos que incluyen un tazón de aluminio o un blindaje de acero inoxidable cumplen con la certificación ASTM FS1201, están listados por UL, se aceptan American Bureau of Shipping, Veritas, Det Norske Veritas, ISO 10088 y USCG.<br>Para un servicio severo, se deben especificar los tazones metálicos.&nbsp;Otros conjuntos de filtración de combustible incluyen filtros / coalescedores de combustible de alto flujo.&nbsp;</p>\r\n<p><strong>TENGA EN CUENTA&nbsp;</strong>:Somos distribuidores oficiales de la gama completa de filtración racor.&nbsp;<br>Si no ve el producto que requiere para la venta en línea, póngase en contacto con nosotros por teléfono o correo electrónico para pedir el artículo.</p>\r\n','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,2,'2018-03-01 16:43:49','2018-03-01 16:47:55','admin','admin',0),
  (41,1,1,3,'','','','','','','','','','','','','','','','','','','','','racor.jpg','','','','','','','','','','','','','','','','','','','','6','','','','','','','','','','','','','','','','','','','',14,8,'2018-03-01 17:02:11','2018-03-22 11:16:09','admin','admin',0),
  (43,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',3,9,'2018-03-01 17:21:44','2018-08-28 17:12:30','admin','admin',0),
  (44,1,1,3,'Ubíquenos','mapa','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',3,4,'2018-03-01 17:25:39','2018-03-24 09:41:52','admin','admin',0),
  (45,1,1,4,'','','','','','','','','','','','','','','','','','','','','keltec2.jpg','','','','','','','','','','','','','','','','','','','','7','','','','','','','','','','','','','','','','','','','',14,8,'2018-03-01 17:42:05','2018-03-23 12:47:36','admin','admin',0),
  (46,1,1,5,'','','','','','','','','','','','','','','','','','','','','baldwin.jpg','','','','','','','','','','','','','','','','','','','','8','','','','','','','','','','','','','','','','','','','',14,8,'2018-03-01 17:49:38','2018-03-01 17:49:38','admin','admin',0),
  (47,1,1,1,'https://www.facebook.com/MultiFiltros-Santa-Cruz-SRL-814193932037003/','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',17,10,'2018-03-02 12:12:41','2018-08-13 10:32:53','admin','admin',0),
  (48,1,1,1,'Productos','Multi Filtros Santa cruz - Bolivia','<p class=\"text-center\"><strong></strong>Av. Nicolás Suárez #1740, 4to Anillo&nbsp;esq. Diego de&nbsp;Trejo<br>frente al Parque Industrial.&nbsp;<a href=\"http://multifiltrossantacruz.com/contactos/#mapa\" class=\"button--secondary button--small\">&gt;&gt; Ver mapa</a><br><strong>Telfs:&nbsp;</strong>(591-3) 3478848 /&nbsp;3493339<br><strong>Cel:&nbsp;</strong><a class=\"whatsapp\" href=\"https://api.whatsapp.com/send?phone=59174174477\" target=\"_blank\">74 17 44 77</a><br>multifiltroscventas@gmail.com</p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','4,5,6,7,8,9,10,20,12','','','','','','','','','',15,11,'2018-03-02 15:06:12','2018-08-13 10:41:19','admin','admin',0),
  (49,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','1,2,3','','','','','','','','','',18,12,'2018-03-10 09:35:50','2018-03-10 09:35:50','admin','admin',0),
  (50,1,1,2,'<p>Desarrolla sus productos dentro de estándares rígidos de calidad, garantizando a sus clientes todas las exigencias que los equipos modernos necesitan.<br></p>\r\n<p>Certificada por la Norma ISO9001, <a href=\"http://www.unifilter.com.br\" target=\"_blank\">Unifilter</a> desarrolla sus productos dentro de estándares rígidos de calidad, garantizando a sus clientes todas las exigencias que los equipos modernos necesitan.</p>\r\n<p><strong>- Pesados:&nbsp;</strong>&nbsp;camiones y autobuses, vehículos utilitarios</p>\r\n<p><strong>- Agricultura:</strong>&nbsp;&nbsp;Maquinaria agrícola</p>\r\n<p><strong>- Maquinaria y Equipo:&nbsp;</strong>&nbsp;Construcción, Producción de energía, carretillas elevadoras, compresores, cabinas de pintura, EDM, bombas de vacío y amoníaco</p>\r\n<p><strong>- Especial:&nbsp;</strong>&nbsp;Filtros especiales</p>\r\n<hr>\r\n<p><strong><u>Línea Industrial&nbsp;</u></strong><br></p>\r\n<p>La línea se constituye en filtros y elementos del aire (tapas de platisol y tapas metálicas), aceite lubricante, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, hidráulico, spin-on alta presión, separadores del aire del aceite aplicados en compresores y coalescentes.</p>\r\n<p><strong><u>Línea Móvil</u></strong></p>\r\n<p>Son Filtros aplicados en Camiones, Autobuses, Pick-ups, Tractores, Cosechadoras, Pulverizadores, equipos y motores impulsados por diesel.&nbsp;La línea se constituye en filtros y elementos del aire (tapas de poliuretano con sellado radial y tapas metálicas con sellado Axial), aceite lubricante, aceite hidráulico, diesel, separadores de agua del diesel y para sistemas de enfriamiento de motores.</p>\r\n<p><strong><u>Línea Marítima<br></u></strong></p>\r\n<p>Son filtros Separadores de agua del diesel fabricados en versiones simples o conjugados, con o sin válvulas By-Pass, para aplicación específica en el mercado Marítimo.&nbsp;La línea se constituye en filtros para motores diesel con una potencia específica entre 140 y 3.600 HP.</p>\r\n<p><strong><u>Línea Hidráulica</u></strong></p>\r\n<p>Son Filtros de baja, media y alta presión utilizados en depósitos, equipos y sistemas hidráulicos en las líneas de presión, succión y retorno.</p>\r\n<p>Todos los filtros utilizan medios filtrantes de tela, celulosa o micro fibra que poseen eficiencia de filtración conforme a la norma ISO 16889 que regula el patrón de limpieza de los fluidos en sustitución de la ISO 4572.</p>\r\n<p><strong><u>Línea Amonia</u></strong></p>\r\n<p>Son filtros utilizados en compresores o líneas de refrigeración.&nbsp;Desarrollados de acuerdo con las principales marcas en el mercado: York, Frick, Sabroe, Mycom, Sthal y otros.&nbsp;Utiliza medio filtrante de alta generación y componentes normalizados para garantizar un producto de alta calidad, dentro de las normas exigidas.</p>\r\n<p><strong><u></u>Línea Compresores</strong></p>\r\n<p>Unifilter fabrica la línea completa de filtros de aire, aceite y separadores aire / aceite para los compresores de las principales marcas en el mercado: Atlas Copco, Chicago Pneumatic, Kaeser, Schulz, Worthington y otros.&nbsp;Desarrollados con alta tecnología, garantizan un menor residual de aceite para la red de aire, mayor capacidad de retención de partículas y menor restricción al flujo de aire y aceite.</p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,2,'2018-03-15 12:19:17','2018-03-16 08:55:03','admin','admin',0),
  (51,1,1,3,'Pesados','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,4,'2018-03-15 12:24:18','2018-03-15 12:24:18','admin','admin',0),
  (52,1,1,4,'<p>Unifilter fabrica la línea completa de filtros para camiones y autobuses, de todas las marcas: Agrale, Chrysler, Fiat, Ford, General Motors, Iveco, Kia Motors, Mercedes Benz, Scania, Volkswagen, Volvo y otros.&nbsp;La línea se constituye en filtros y elementos del aire (con tapas de poliuretano y sellado radial o tapas metálicas y sellado axial);&nbsp;filtros lubricantes e hidráulicos;&nbsp;filtros de combustible (diesel);&nbsp;separadores de agua del diesel y filtros para sistemas de refrigeración de motores.&nbsp;Fabricados con tecnología avanzada garantizan la alta eficiencia requerida en los equipos actuales.</p>','','','','','','','','','','','','','','','','','','','','001.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:28:55','2018-03-15 12:29:21','admin','admin',0),
  (53,1,1,5,'<p>Los filtros para líneas de Vans, Camionetas y Vucs para diversas marcas: Agrale, Asia Motors, Fiat, GM, Iveco, Mercedes Benz, Toyota, y otros Se constituyen en filtros y elementos del aire, filtros lubricantes, filtros del combustible (diesel ) y separadores de agua del diesel.&nbsp;Fabricados con tecnología avanzada garantizan la alta eficiencia requerida en los equipos actuales.</p>','','','','','','','','','','','','','','','','','','','','002.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:31:44','2018-03-15 12:31:44','admin','admin',0),
  (54,1,1,7,'Máquinas agrícolas','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,4,'2018-03-15 12:33:19','2018-03-15 12:33:19','admin','admin',0),
  (55,1,1,8,'<p>Son filtros aplicados en Tractores, Cosechadoras, Pulverizadores, etc.&nbsp;Modelos desarrollados para diversas marcas: Agrale, Case, Caterpillar, John Deere, New Holland, Valtra y otros.</p>\r\n<p>La línea se constituye en filtros y elementos del aire (con tapas de poliuretano y sellado radial o tapas metálicas y sellado axial);&nbsp;filtros lubricantes e hidráulicos;&nbsp;filtros de combustible (diesel);&nbsp;separadores de agua del diesel y filtros para sistemas de refrigeración de motores.&nbsp;Fabricados con tecnología avanzada garantizan la alta eficiencia requerida en los equipos actuales.</p><p></p>','','','','','','','','','','','','','','','','','','','','003.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:37:14','2018-03-15 12:37:14','admin','admin',0),
  (56,1,1,9,'Maquinaria y Equipo','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,4,'2018-03-15 12:40:38','2018-03-15 12:40:38','admin','admin',0),
  (57,1,1,10,'<p>Los filtros utilizados en Tractores, Excavadoras, Compactadores, Grúas, etc., de diversas marcas como: Case, Caterpillar, BobCat, Dynapac, JCB, John Deere, Komatsu y otros.&nbsp;La línea se constituye en filtros y elementos del aire (con tapas de poliuretano y sellado radial o tapas metálicas y sellado axial);&nbsp;filtros lubricantes e hidráulicos;&nbsp;filtros de combustible (diesel);&nbsp;separadores de agua del diesel y filtros para sistemas de refrigeración de motores.&nbsp;Fabricados con tecnología avanzada garantizan la alta eficiencia requerida en los equipos actuales.</p>','','','','','','','','','','','','','','','','','','','','004.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:48:33','2018-03-15 12:48:33','admin','admin',0),
  (58,1,1,11,'<p>Unifilter desarrolla y fabrica filtros para las Industrias de Base y Transformación, Son filtros utilizados en varias fases de los procesos: aire, aceite, gases, etc.&nbsp;Utilizan medios filtrantes especificados de acuerdo con la necesidad y aplicación: celulosa, telas metálicas, fibras sintéticas, carbón activado, etc.</p>','','','','','','','','','','','','','','','','','','','','005.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:49:15','2018-03-15 12:49:15','admin','admin',0),
  (59,1,1,12,'<p>Son filtros utilizados en Apiladoras de diversas marcas como: Hyster, Yale, Clark, Linde, etc.&nbsp;La línea se constituye en filtros y elementos del aire (con tapas de poliuretano y sellado radial o tapas metálicas y sellado axial);&nbsp;filtros lubricantes, hidráulicos y del combustible.&nbsp;Fabricados con tecnología avanzada garantizan la alta eficiencia requerida en los equipos actuales.</p>','','','','','','','','','','','','','','','','','','','','multifiltros-logo_1.png','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:49:40','2018-03-15 12:49:40','admin','admin',0),
  (60,1,1,13,'<p>Son filtros para Cabinas de Pintura, Despojamiento y chorreado.&nbsp;Para reposición de las más diversas marcas: Durr, Erzinger, Torit, etc.&nbsp;Poseen tapas metálicas y medio filtrantes con anclaje de los pliegues.&nbsp;Medio filtrante en celulosa o poliéster con alta eficiencia de filtración.</p>','','','','','','','','','','','','','','','','','','','','006.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:49:58','2018-03-15 12:49:58','admin','admin',0),
  (61,1,1,14,'<p>Son filtros para retirar el residuo del fluido dieléctrico provocado por el corte o electroerosión en las máquinas EDM por hilo y por penetración.&nbsp;El elemento Unifilter ha sido utilizado con éxito durante años en la filtración fina de estos líquidos y en la desionización del agua.&nbsp;Poseen medio filtrante de alta generación, para mantener las características originales de los productos.&nbsp;Construcción metálica y / o plástica.&nbsp;Partes metálicas pintadas conforme a las normas internacionales exigidas por los fabricantes de máquinas EDM.&nbsp;Materiales utilizados que no presentan influencia química en el fluido dieléctrico.&nbsp;Proporcionando mayor eficiencia del equipo.<br></p>','','','','','','','','','','','','','','','','','','','','007.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:50:34','2018-03-15 12:50:34','admin','admin',0),
  (62,1,1,15,'','','','','','','','','','','','','','','','','','','','','008.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:50:47','2018-03-15 12:50:47','admin','admin',0),
  (63,1,1,16,'Filtros especiales','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,4,'2018-03-15 12:51:04','2018-03-15 12:51:26','admin','admin',0),
  (64,1,1,17,'<p>Unifilter desarrolla y fabrica filtros especiales de acuerdo con las necesidades y especificaciones de los clientes y / o según las muestras.</p>\r\n<p>En nuestro laboratorio propio, analizamos los medios filtrantes para garantizar un producto desarrollado con las características idénticas a la muestra recibida.</p>','','','','','','','','','','','','','','','','','','','','009.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-15 12:51:51','2018-03-15 12:51:51','admin','admin',0),
  (65,1,1,6,'<p>El filtro de combustible se conecta el motor con el depósito de combustible y se clasifica como un elemento de seguridad.&nbsp;Su función es la de no permitir que los fallos en el motor para el suministro de combustible de manera que no es la interrupción de su funcionamiento.&nbsp;Este tipo de filtro asegura una vida más larga para el sistema de inyección y la carburación, la eliminación de partículas no deseadas tales como polvo, óxido y depósito de residuos.</p>','','','','','','','','','','','','','','','','','','','','03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-03-16 09:10:24','2018-03-16 09:19:02','admin','admin',0),
  (66,1,1,7,'<p>Este filtro está totalmente conectado a la salud de los ocupantes de los vehículos.&nbsp;Su principal función es retener impurezas en el aire tales como polvo, hollín y los gases, asegurando que el filtro de aire alcanza el interior del vehículo.&nbsp;Este producto ha recibido un alto grado de importancia en vehículos, ya sea ligero o pesado.&nbsp;Una de las tecnologías incorporadas en esta familia de filtros es el \"tratamiento de corona\".&nbsp;Esta tecnología permite aplicar controlado cargas eléctricas en los medios plana sintética que generan un campo electrostático en medio de ellos que aumentarán el poder de capturar las partículas en su funcionamiento a principios del sistema de aire acondicionado de vehículos.</p>','','','','','','','','','','','','','','','','','','','','04.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-03-16 09:10:55','2018-03-16 09:20:53','admin','admin',0),
  (67,1,1,8,'<p>En el caso de los filtros de aceite, que llamamos proyectos ecológicos que incluyen soluciones y / o propuestas para materiales alternativos con la posibilidad de reciclar el bajo nivel de residuo final.&nbsp;Los ejemplos incluyen tecnologías de medios de filtro con la reducción de papel y pulpa, la ampliación de los materiales sintéticos como el polipropileno, poliéster, medios de comunicación multi-capa con material compuesto de dos materiales, los conceptos innovadores que eliminar elementos metálicos y sustituidos por polímeros inyectados y propuestas de proyectos con el rendimiento de generación de mantenimiento \"limpia\" y bajo volumen de material para su eliminación.&nbsp;Las relaciones de uso o aplicación de estos filtros, ya sea línea suave o duro, está sujeto a la instrucción del proveedor y la especificación del fabricante de automóviles.&nbsp;Los filtros ecológicos han estado disponibles desde finales de los años 90 en Europa y Asia con el arranque del motor con inyección electrónica.&nbsp;En Brasil este paradigma se rompe con la entrada de los vehículos importados de alta litragem y ahora con el aumento del volumen de las marcas asiáticas, obligando a otros fabricantes para dirigir sus proyectos para este tipo de tecnología.</p>','','','','','','','','','','','','','','','','','','','','05.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-03-16 09:11:33','2018-03-16 09:21:27','admin','admin',0),
  (68,1,1,9,'<p>Operando en el sistema de suministro de combustible del motor, que tiene funciones específicas para separar el agua presente en diésel.&nbsp;La baja eficiencia de la separación de este agua puede, dependiendo de la posición de montaje, reducir la vida útil de la bomba de inyección, colaborar con la aparición de corrosión en los componentes a la pérdida del motor.</p>','','','','','','','','','','','','','','','','','','','','06.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-03-16 09:11:55','2018-03-16 09:22:03','admin','admin',0),
  (69,1,1,10,'<p>Tiene el propósito de eliminar la humedad constante en la línea de aire de freno del vehículo, a partir de la captura de aire atmosférico por el compresor.&nbsp;La baja eficiencia de este filtro resulta en la pérdida funcional del sistema de frenos.&nbsp;Actualmente, los camiones más nuevos ya tienen un sistema de seguridad que bloquea el vehículo cuando la pérdida de la funcionalidad del sistema de frenos.</p>','','','','','','','','','','','','','','','','','','','','07.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-03-16 09:12:13','2018-03-16 09:23:12','admin','admin',0),
  (70,1,1,11,'<p>Los filtros TECMAX tener la misma calidad de los filtros convencionales y sólo cinco tipos tienen más de 150 aplicaciones.&nbsp;Esta tecnología sólo fue posible a partir de estudios de las diferentes características de los filtros utilizados por diferentes fabricantes, agrupación que tenía características y tecnologías similares.&nbsp;Una línea producida especialmente para aquellos con poco margen para la acción, pero no compromete la calidad.&nbsp;Además, los filtros TECMAX tienen un coste menor que los filtros convencionales.</p>','','','','','','','','','','','','','','','','','','','','08.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-03-16 09:12:32','2018-03-16 09:23:40','admin','admin',0),
  (71,1,1,6,'Filtros de combustible de motores','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,4,'2018-03-16 12:43:32','2018-03-16 16:07:59','admin','admin',0),
  (83,1,1,7,'<p>El separador de agua del filtro de combustible elimina las partículas y el agua de su combustible, proporcionándole la tranquilidad necesaria cuando la falla del motor no es una opción.<br></p>','','','','','','','','','','','','','','','','','','','','01_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:49:30','2018-03-16 16:01:13','admin','admin',0),
  (84,1,1,9,'<p>Los filtros de la serie Racor Diesel Spin-on utilizan medios Aquabloc® para eliminar de manera confiable la suciedad y el agua de los combustibles diesel y de gasolina.&nbsp;Los medios Aquabloc® están plisados, corrugados y dispuestos para un rechazo óptimo del agua y una vida útil más prolongada.<br></p>','','','','','','','','','','','','','','','','','','','','03_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:49:53','2018-03-16 16:02:16','admin','admin',0),
  (85,1,1,8,'<p>El SNAPP &trade; Marine, Engine, Mobile Fuel Filter / Water Separator es un filtro compacto de servicio rápido con capacidades de filtración de primera calidad.<br></p>','','','','','','','','','','','','','','','','','','','','02_3.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:50:13','2018-03-16 16:01:43','admin','admin',0),
  (86,1,1,10,'<p>Los conjuntos de filtro FBO Diesel limpios están diseñados para cumplir con las condiciones de reabastecimiento de combustible marinas e industriales más duras, a la vez que proporcionan cambios de cartucho fáciles.<br></p>','','','','','','','','','','','','','','','','','','','','04_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:50:50','2018-03-16 16:02:40','admin','admin',0),
  (87,1,1,11,'<p>El Racor GreenMax es el separador de agua / filtro de combustible diésel más innovador del mundo para todas las operaciones meteorológicas.<br></p>','','','','','','','','','','','','','','','','','','','','05_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:51:08','2018-03-16 16:03:16','admin','admin',0),
  (88,1,1,12,'<p>Sistemas integrados de bomba / separador de combustible para motores diésel actuales y aplicaciones de pulido de combustible.<br></p>','','','','','','','','','','','','','','','','','','','','06_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:51:28','2018-03-16 16:03:41','admin','admin',0),
  (89,1,1,13,'<p>Los Prefiltros y Filtros de trabajo pesado y compacto de Racor se instalan rápidamente y eliminan contaminantes sólidos y agua libre de gasolina y combustible diesel en aplicaciones de motores pequeños.<br></p>','','','','','','','','','','','','','','','','','','','','07_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:52:19','2018-03-16 16:04:04','admin','admin',0),
  (90,1,1,14,'<p>La serie 110A es un separador de agua / filtro de combustible de aluminio, de dos piezas, de alta presión, con un elemento de filtro tipo cartucho.<br></p>','','','','','','','','','','','','','','','','','','','','08_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:52:41','2018-03-16 16:06:05','admin','admin',0),
  (91,1,1,15,'<p>Sistema integrado de bomba de combustible / separador para aplicaciones de pulido de combustible.<br></p>','','','','','','','','','','','','','','','','','','','','09.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:53:08','2018-03-16 16:06:35','admin','admin',0),
  (92,1,1,16,'<p>Sistemas integrados de bomba de filtro / separador de combustible para motores diésel actuales<br></p>','','','','','','','','','','','','','','','','','','','','10.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-16 15:53:27','2018-03-16 16:07:03','admin','admin',0),
  (94,1,1,12,'<p>Los filtros TECMAX tener la misma calidad de los filtros convencionales y sólo cinco tipos tienen más de 150 aplicaciones.&nbsp;Esta tecnología sólo fue posible a partir de estudios de las diferentes características de los filtros utilizados por diferentes fabricantes, agrupación que tenía características y tecnologías similares.&nbsp;Una línea producida especialmente para aquellos con poco margen para la acción, pero no compromete la calidad.&nbsp;Además, los filtros TECMAX tienen un coste menor que los filtros convencionales.</p>','','','','','','','','','','','','','','','','','','','','08.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,5,'2018-03-21 12:52:33','2018-03-21 12:52:33','admin-multifiltros','admin-multifiltros',0),
  (95,1,1,2,'<p><strong>Baldwin tiene larga trayectoria,&nbsp;</strong>y abundante tradición, desde un comienzo pequeño con menos de una docena de empleados hasta una empresa mundial con distribución a lo largo de seis continentes, Baldwin Filters ha crecido y prosperado desde su fundación.&nbsp;</p>\r\n<p><strong>La calidad es la máxima prioridad en Baldwin Filters.</strong></p>\r\n<p>El equipo de ingenieros de Baldwin busca continuamente métodos para mejorar nuestro producto. Trabajando desde el diseño básico de filtración y utilizando la tecnología más moderna, tal como la creación de modelos en 3-D mediante CAD y la elaboración de prototipos estéreo litográficos, el grupo de ingeniería muestra el camino a seguir con diseños innovadores y patentados, tal como los filtros de aire de Sello Radial, las válvulas de drenaje de auto venteo para los separadores de combustible/agua y los filtros de refrigerante de liberación controlada. Nuestra meta es diseñar filtros que cumplan o superen las especificaciones de los Fabricantes de Equipos Originales (OEM). Todos los filtros Baldwin se analizan en nuestro moderno centro técnico y se someten a pruebas prácticas extensas.&nbsp; La calidad es la máxima prioridad de Baldwin Filters. Baldwin Filters no es simplemente una ensambladora de filtros como muchas compañías grandes. Un proceso de fabricación verticalmente integrado permite a Baldwin lograr una consistencia y un control óptimos, ya que no solamente producen filtros sino que también fabrican componentes para filtros. La práctica de fabricar un filtro de principio a fin permite ofrecer más calidad y control de costos, lo cual da a Baldwin Filters una ventaja sobre sus competidores. Para respaldar su compromiso con la calidad, Baldwin Filters ha logrado y mantiene las certificaciones TS 16949 e ISO 9001 en sus plantas.</p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',8,2,'2018-03-21 17:21:14','2018-03-21 17:21:14','admin-multifiltros','admin-multifiltros',0),
  (96,1,1,1,'Contáctenos','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',3,4,'2018-03-22 12:13:46','2018-03-22 12:13:46','admin','admin',0),
  (97,1,1,2,'HISTORIA','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',2,4,'2018-03-28 11:59:00','2018-03-28 11:59:00','admin','admin',0),
  (98,1,1,4,'Misión','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',2,4,'2018-03-28 11:59:16','2018-03-28 11:59:16','admin','admin',0),
  (99,1,1,6,'Visión','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',2,4,'2018-03-28 11:59:25','2018-03-28 11:59:25','admin','admin',0),
  (100,1,1,3,'<p>La empresa fue fundada viendo la necesidad del mercado, en el año 1999 , con el objetivo de brindar soluciones &nbsp;a los requerimientos de los clientes de este Paìs.</p>\r\n<p>La empresa se dedica &nbsp;a la Importcaciòn y Comercializaciòn de filtros.</p>\r\n<p>Tiene una red de Distribuidores y sub distribuidores en todo el territorio nacional, abarcando el mayor porcentaje de clientes , en relación a las otras empresas que se dedican al mismo rubro</p>\r\n','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',2,2,'2018-03-28 12:00:02','2018-03-28 12:00:02','admin','admin',0),
  (101,1,1,5,'<p>Somos una empresa dedicada a atender las necesidades de filtros en el mercado automotor , Agrîcola ,Industrial y Minero, a través del mejor equipo humano, brindando un excelente servicio de venta, &nbsp;a nuestros clientes. Somos distribuidores de las mejores marcas de Filtros, de muy buena calidad y con precios muy competitivos.<br></p>\r\n<p>Más que vendedores brindamos Soluciones en filtración</p>\r\n','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',2,2,'2018-03-28 12:01:20','2018-03-28 12:01:20','admin','admin',0),
  (102,1,1,7,'<p>Ser la mejor empresa importadora y comercializadora de Filtros automotrices en Bolivia, brindando un excelente servicio en venta y &nbsp;&nbsp;generando valor agregado para nuestros clientes y la sociedad.</p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',2,2,'2018-03-28 12:01:42','2018-03-28 12:01:42','admin','admin',0),
  (103,1,1,1,'<p>Especializados en la producción de filtros agrícolas y ampliando su línea para los filtros de camiones y motores estacionarios.&nbsp;</p>','','','','','','','','','','','','','','','','','','','','tbfil.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',20,3,'2018-03-28 12:06:12','2018-08-24 11:12:34','admin','admin',0),
  (105,1,1,3,'<p><strong></strong>La serie Maxiflow es una gama de filtros giratorios diseñados para proporcionar protección de bajo costo y calidad para sistemas hidráulicos o de lubricación.&nbsp;\r\nPresión máxima 10 bar.&nbsp;Flujo máximo 360 l/min.</p>','','','','','','','','','','','','','','','','','','','','racor01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-29 10:44:31','2018-03-29 10:51:11','admin','admin',0),
  (106,1,1,4,'<p>El uso de las latas giratorias de reemplazo Parker 12AT y 50AT garantiza la calidad de filtración de Parker.&nbsp;El uso de filtros de posventa con calidad de medios desconocidos puede ahorrar costos iniciales, pero puede aumentar los costos generales al requerir más cambios de elementos, más desperdicio y potencialmente causar un tiempo de inactividad del sistema.&nbsp;Proteja su inversión comprando siempre elementos genuinos de reemplazo de Parker.&nbsp;\r\nDisponibles en longitudes única y doble con varias clasificaciones de micras (3,10, 20, 25) y opciones de medios (Microglass y celulosa), estas latas giratorias de repuesto aseguran la calidad de la filtración por la que Parker es conocida.&nbsp;Para realizar el pedido, consulte la sección \"Cómo realizar un pedido\" de la literatura o comuníquese con un Representante de Parker.</p>','','','','','','','','','','','','','','','','','','','','racor02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-29 10:53:17','2018-03-29 10:53:45','admin','admin',0),
  (107,1,1,5,'','','','','','','','','','','','','','','','','','','','','racor03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,5,'2018-03-29 10:55:51','2018-03-29 10:59:01','admin','admin',0),
  (108,1,1,3,'<p>Los filtros de aire EnduraPanel están diseñados para durar mientras brindan un rendimiento superioreficiencia a lo largo de todo el intervalo de servicio. La tenencia de tierrala capacidad supera las ofertas de filtro OE. El compacto de EnduraPanellos diseños brindan flexibilidad para acomodar varios motoresconfiguraciones Las pruebas de resistencia estructural prueban una vez más queBaldwin cumple y excede el producto OE. El PA31010 (primario)y el PA31011 (secundario), proporcionan una excelente protección para suBobcat Cargadores y manipuladores telescópicos con Doosan y Kubota<br></p>','','','','','','','','','','','','','','','','','','','','balwin01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',8,5,'2018-03-29 11:09:31','2018-03-29 11:09:31','admin','admin',0),
  (109,1,1,4,'','','','','','','','','','','','','','','','','','','','','balwin02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',8,5,'2018-03-29 11:14:31','2018-03-29 11:15:48','admin','admin',0),
  (110,1,1,5,'<p>Un filtro hidráulico es el componente principal del sistema de filtración de una&nbsp;máquina hidráulica, de lubricación o de engrase. Estos sistemas se emplean para el control de la contaminación por partículas sólidas de origen externo y las generadas internamente por procesos de desgaste o de erosión de las superficies de la maquinaria, permitiendo preservar la vida útil tanto de los componentes del equipo como del fluido hidráulico. Por medio de los filtros un equipo se mantiene en buen estado, aumentando su vida útil.</p>','','','','','','','','','','','','','','','','','','','','balwin03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',8,5,'2018-03-29 11:18:13','2018-03-29 11:18:13','admin','admin',0),
  (111,1,1,2,'<p><strong>FCD-311</strong>&nbsp;DUOFILTRO DE COMBUSTIBLE<br>COMPLETO ARMADO P/ MOTORES DEUTZ<strong>&nbsp;(Nº ORIG. 302 0984 )</strong>&nbsp;C/ ELEM. FILTRANTE Y Y VASO DECANTADOR Y APLIC. VARIAS&nbsp;</p>','','','','','','','','','','','','','','','','','','','','mc-repuesto01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',9,5,'2018-03-29 11:56:26','2018-03-29 11:56:26','admin','admin',0),
  (112,1,1,3,'<p><strong>FCD-312</strong>&nbsp;DUOFILTRO DE COMBUSTIBLE<br>COMPLETO ARMADO SIMILAR AL FCD-304<br>CON VASOS CONICOS Y ROSCA 14 X 1.50<br>PARA APLICACIONES VARIAS&nbsp;</p>','','','','','','','','','','','','','','','','','','','','mc-repuesto02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',9,5,'2018-03-29 11:57:52','2018-03-29 11:57:52','admin','admin',0),
  (113,1,1,4,'<p><strong>FCD-301&nbsp;</strong>DUOFILTRO PERKINS ARMADO COMPLETO, CON ELEMENTOS FILTRANTES, Y VASOS DE ALUMINIO MOTORES 6-354 FII<br><strong>FCD-301/1</strong>&nbsp;IDEM AL ANTERIOR SIN VASO<br>SIMILAR A CONJUNTO ORIGINAL&nbsp;<strong>Nº 005310</strong><br><strong>FCD-301/2&nbsp;</strong>ARMADO CON VASO PLASTICO<br><strong>FCD-301/3</strong>&nbsp;CON VASO “ESPECIAL“ o VIDRIO<br><strong>FCD-301/4&nbsp;</strong>ARMADO CON TAPA&nbsp;<strong>D-101/1&nbsp;</strong><br>SIMILAR A CONJUNTO ORIGINAL&nbsp;<strong>Nº 005311</strong></p>','','','','','','','','','','','','','','','','','','','','mc-repuesto03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',9,5,'2018-03-29 11:59:14','2018-03-29 11:59:14','admin','admin',0),
  (114,1,1,5,'<p><strong>VV-02</strong>&nbsp;VASO TIPO CAV PARA FILTROS<br><strong>D-141</strong>&nbsp;-- PERKINS – DEUTZ – M. DIESEL&nbsp;<br>M. FERGUSON - BEDFORD Y AP. VARIAS</p>','','','','','','','','','','','','','','','','','','','','mc-repuesto04.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',9,5,'2018-03-29 12:00:52','2018-03-29 12:00:52','admin','admin',0),
  (115,1,1,7,'','','','','','','','','','','','','','','','','','','','','tbfil01.jpg','','','','','','','','','','','','','','','','','','','','20','','','','','','','','','','','','','','','','','','','',14,8,'2018-03-29 12:06:31','2018-08-10 17:23:53','admin','admin',0),
  (116,1,1,5,'<p>Filtros de aire son producidos con medio filtrante de resina de celulosa impregnada o poliéster sintético, con tapaderas de plastisol (goma) o de construcción en metal, así como también los de última generación libres de metal.</p>\r\n<p>• Filtros de aire para compresores<br>• Filtros de aire para vacío<br>• Separadores de aceite para vacío</p><p></p>','','','','','','','','','','','','','','','','','','','','keltec04.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',7,5,'2018-03-29 12:36:23','2018-03-29 12:38:12','admin','admin',0),
  (117,1,1,4,'<p>Filtro Separador Ar/Óleo Similar Kaeser SM7,5, SM10, SM15, N15 Cod: 637930</p>','','','','','','','','','','','','','','','','','','','','keltec02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',7,5,'2018-03-29 12:38:38','2018-03-29 12:39:08','admin','admin',0),
  (119,1,1,6,'','','','','','','','','','','','','','','','','','','','','unifilter01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-29 18:02:31','2018-03-31 11:25:41','admin','admin',0),
  (120,1,1,18,'Lanzamientos','#lanzamientos','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,4,'2018-03-31 10:58:29','2018-04-02 12:47:43','admin','admin',0),
  (121,1,1,19,'<p><strong>CONVERSIÓN DIRECTA</strong></p><ul><li>BALDWIN - RS5748</li><li>BOBCAT - 7008044</li><li>FLEETGUARD - AF27999</li></ul>\r\n<p><strong>APLICACIÓN</strong><br>MINI CARGADORA BOBCAT S630 / SERIE S / KUBOTA V1307DIT<br>MINI CARGADORA BOBCAT T650 / SERIE T / DOOSAN D24<br>MINI CARGADORA BOBCAT 365C / SERIE S / KUBOTA V1303OIT MINI CARGADORA BOBCAT T630 / SERIE T / KUBOTA V1307OIT</p>','','','','','','','','','','','','','','','','','','','','la03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-31 11:02:34','2018-04-02 11:03:38','admin','admin',0),
  (122,1,1,20,'<p><strong>CONVERSIÓN DIRECTA</strong></p><ul><li>BALDWIN - PF7655</li><li>DUNALUSUN - FS5111 /</li><li>FLEETGUARD FEE123</li><li>WIX - 33S92</li></ul><p><strong>ORIGINAL</strong> CATERPILLAR 16075/160718 / SN9e50</p><p><strong>APLICACIÓN</strong><br>CATERPILLAR 35C8B / 1510 / PV3512: 3400 / FM3516 / 3512B / FM3508<br>IAMRIX: K DYUKS XGMA XC904</p>','','','','','','','','','','','','','','','','','','','','la01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-31 11:04:45','2018-04-02 11:23:10','admin','admin',0),
  (123,1,1,21,'<p><strong>CONVERSIÓN DIRECTA</strong><br></p><ul><li>RAIVYN - HS10133</li><li>PARKER -500375001</li><li>WIX - WA10043</li></ul>\r\n<p><strong>ORIGINAL</strong><br>FCRD - DC469601AA</p>\r\n<p><strong>APLICACIÓN</strong><br>CAMIONES FORD CARGO<br>C2042 / C2642</p>','','','','','','','','','','','','','','','','','','','','la05.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-31 11:08:07','2018-04-02 11:31:38','admin','admin',0),
  (124,1,1,22,'<p><strong>CONVERSIÓN DIRECTA</strong></p>\r\n<ul><li>KELTEC - KD125064</li><li>ATLAS COPCO - 2911016000</li></ul>\r\n<p><strong>APLICACIÓN</strong></p><p>ATLAS COPCO - XA597D0 / XAS127 / YAS137 / XA147 / XAS177 / XAS1BTPD CHICAGO - 3000</p>','','','','','','','','','','','','','','','','','','','','la06.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-31 11:09:38','2018-04-02 11:36:41','admin','admin',0),
  (125,1,1,23,'<p><strong>CONVERSIÓN DIRECTA</strong></p><ul><li>BALDWIN - PT94D9MPG</li><li>DONALDS0N - P568836</li><li>FLEETGUARD - HF35343</li><li>WIX - 577E5</li></ul>\r\n<p><strong>ORIGINAL</strong><br>CHN DEERE - AL203C59 / AL203061 / AL16C316 / AL160771</p>\r\n<p><strong>APLICACIÓN</strong><br>CUBIERTA DE CANA JOHN DEERE - T570<br>TRATORES JOHN DEERE - 6120 / 6120L / 6215/6220 / 6220L / 6320 / 6320L / 6415/64200/6200/6520 / 6520N S52TSE 86156715/7220 773 2074 207520</p>','','','','','','','','','','','','','','','','','','','','la02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-31 11:13:17','2018-04-02 12:29:13','admin','admin',0),
  (126,1,1,24,'<p><strong>CONVERSIÓN DIRECTA</strong></p>\r\n<ul><li>BALDWIN RS5747</li><li>BOBCAT 7008043</li><li>FLEETGUARD AF27998</li></ul>\r\n<p><strong>APLICACIÓN</strong></p>\r\n<p>MINI CARGADORA BOBCAT S630 / SERIE S / KUВОТА УСП</p><p>MINI CARGADORA BOBCAT T650 / SERIE T / DOOSAN D24</p><p>MINI CARGADORA BOBCAT S650 / SÉRIE S / KUВОТА V1307OT</p><p>MINI CARGADORA BOBCAT 1630 / SERIE T / KUВОТА V3307OIT</p>','','','','','','','','','','','','','','','','','','','','la04.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-31 11:14:11','2018-04-02 12:39:14','admin','admin',0),
  (127,1,1,25,'<p><strong>CONVERSIÓN DIRECTA</strong><br></p>\r\n<ul><li>KELTEC KV775013</li></ul>\r\n<p><strong>ORIGINAL</strong></p><ul><li>KAESER 635590</li></ul>\r\n<p><strong>APLICACIÓN</strong><br>COMPRESSORES KAESER SFC200 / ES0200 / ESD250 / ESD251 / ESD300 / ESD301 / HSD500 / HSD550 / HSD600 / HSD65) / HSD711 / HSD741 / HS0831</p>','','','','','','','','','','','','','','','','','','','','la07.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,5,'2018-03-31 11:15:15','2018-04-02 12:46:53','admin','admin',0),
  (132,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','poppup01.jpg,poppup03.jpg,poppup02.jpg,poppup04.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',21,14,'2018-08-13 12:16:34','2018-08-13 12:16:34','admin','admin',0),
  (133,1,1,1,'http://www.unifilter.com.br/','','','','','','','','','','','','','','','','','','','','images.png','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',22,15,'2018-08-24 12:44:08','2018-08-27 10:01:00','admin','admin',0),
  (134,1,1,4,'http://www.tbfil.com.br/','','','','','','','','','','','','','','','','','','','','logo.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',22,15,'2018-08-24 12:44:57','2018-08-27 10:02:17','admin','admin',0),
  (135,1,1,2,'https://keltec.ind.br/pt/','','','','','','','','','','','','','','','','','','','','keltec-technolab-2.png','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',22,15,'2018-08-24 12:45:07','2018-08-27 10:00:04','admin','admin',0),
  (136,1,1,3,'http://www.baldwinfilter.com/es/','','','','','','','','','','','','','','','','','','','','bfbrand1014.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',22,15,'2018-08-24 12:45:17','2018-08-27 10:01:41','admin','admin',0),
  (138,1,1,5,'http://www.tecfil.com.br/','','','','','','','','','','','','','','','','','','','','logo-tecfil.png','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',22,15,'2018-08-24 16:41:16','2018-08-27 10:03:03','admin','admin',0),
  (139,1,1,6,'http://www.parker.com/portal/site/PARKER/menuitem.223a4a3cce02eb6315731910237ad1ca/?vgnextoid=0a9dd7196965e210VgnVCM10000048021dacRCRD&vgnextfmt=ES','','','','','','','','','','','','','','','','','','','','parker-racor-logo_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',22,15,'2018-08-24 16:49:41','2018-08-27 10:05:43','admin','admin',0),
  (140,1,1,7,'https://multifiltrossantacruz.com/mc-repuestos/','','','','','','','','','','','','','','','','','','','','mc.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',22,15,'2018-08-24 16:55:24','2018-08-27 10:05:01','admin','admin',0),
  (141,1,1,8,'https://multifiltrossantacruz.com/rama/','','','','','','','','','','','','','','','','','','','','rama.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',22,15,'2018-08-27 08:47:08','2018-08-27 10:05:16','admin','admin',0),
  (142,1,1,6,'','','','','','','','','','','','','','','','','','','','','slide-rama.jpg','','','','','','','','','','','','','','','','','','','','10','','','','','','','','','','','','','','','','','','','',14,8,'2018-08-27 09:39:39','2018-08-27 09:40:58','admin','admin',0),
  (143,1,1,1,'<p>Filtros de combustible diesel - Separadores de agua</p>','','','','','','','','','','','','','','','','','','','','banner-rama.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',10,3,'2018-08-27 09:46:30','2018-08-27 09:52:26','admin','admin',0),
  (144,1,1,2,'<p><span class=\"contacto\">En el año 1972 <a href=\"http://www.ramamercosur.com\" target=\"_blank\">Rama</a> comenzó a fabricar filtros automotores, 10 años más tarde&nbsp;</span><span class=\"contacto\">fueron distinguidos por RACOR INDUSTRIES INC. de Modesto, USA con la transferencia tecnológica y licencia de uso de marca para fabricar en Argentina todos los filtros de sus series Turbina, 220 y 225.</span></p>\r\n<p><span class=\"contacto\">Actualmente <a href=\"http://www.ramamercosur.com\" target=\"_blank\">Rama</a> son especialistas en la fabricación y exportación de filtros de combustible diesel y separadores de agua; produciendo modelos con diseños patentados, así como marcas propias. Su amplia gama de diseños y la calidad de confección respaldada por estándares internacionales, ubicandolós dentro de las empresas más reconocidas del rubro a nivel mundial.</span></p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',10,2,'2018-08-27 17:37:38','2018-08-27 17:41:55','admin','admin',0),
  (145,1,1,4,'<ul><li><strong>Elemento de reposición:&nbsp;</strong>RK16.</li><li><strong>Rosca de entrada y salida:</strong>&nbsp;m16x1,5.</li><li><strong>Alto máximo:</strong>&nbsp;220mm.</li><li><strong>Ancho maximo:</strong>&nbsp;170mm.</li><li><strong>Profundidad máximo:</strong>&nbsp;115 mm.</li><li><strong>Vacio maximo:</strong>&nbsp;730 mm/hg.</li></ul>','','','','','','','','','','','','','','','','','','','','rama02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',10,5,'2018-08-27 17:48:43','2018-08-28 16:35:32','admin','admin',0),
  (146,1,1,5,'<ul><li><strong>Elemento de reposición:&nbsp;</strong>RK18.</li><li><strong>Rosca de entrada y salida:</strong>&nbsp;m16x1,5.</li><li><strong>Alto máximo:</strong>&nbsp;280mm.</li><li><strong>Ancho maximo:</strong>&nbsp;170mm.</li><li><strong>Profundidad máximo:</strong>&nbsp;115 mm.</li><li><strong>Vacio maximo:</strong>&nbsp;730 mm/hg.</li></ul>','','','','','','','','','','','','','','','','','','','','rama04.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',10,5,'2018-08-27 17:48:50','2018-08-28 16:36:31','admin','admin',0),
  (147,1,1,3,'<ul><li><strong>Elemento de reposición:&nbsp;</strong>RK15.</li><li><strong>Rosca de entrada y salida:</strong> m16x1,5.</li><li><strong>Alto máximo:</strong> 190mm.</li><li><strong>Ancho maximo:</strong> 170mm.</li><li><strong>Profundidad máximo:</strong>&nbsp;115 mm.</li><li><strong>Vacio maximo:</strong> 730 mm/hg.</li></ul>','','','','','','','','','','','','','','','','','','','','rama03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',10,5,'2018-08-27 17:48:54','2018-08-28 16:35:50','admin','admin',0),
  (148,1,1,6,'<ul><li><strong>Elemento de reposición:&nbsp;</strong>RK19.</li><li><strong>Rosca de entrada y salida:</strong>&nbsp;m16x1,5.</li><li><strong>Alto máximo:</strong>&nbsp;340mm.</li><li><strong>Ancho maximo:</strong>&nbsp;170mm.</li><li><strong>Profundidad máximo:</strong>&nbsp;115 mm.</li><li><strong>Vacio maximo:</strong>&nbsp;730 mm/hg.</li></ul>','','','','','','','','','','','','','','','','','','','','rama01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',10,5,'2018-08-27 17:48:58','2018-08-28 16:37:08','admin','admin',0);
/*!40000 ALTER TABLE `rex_article_slice` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_clang`;
CREATE TABLE `rex_clang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_clang` WRITE;
/*!40000 ALTER TABLE `rex_clang` DISABLE KEYS */;
INSERT INTO `rex_clang` VALUES 
  (1,'es','español',1,1,0);
/*!40000 ALTER TABLE `rex_clang` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_config`;
CREATE TABLE `rex_config` (
  `namespace` varchar(75) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`namespace`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rex_config` WRITE;
/*!40000 ALTER TABLE `rex_config` DISABLE KEYS */;
INSERT INTO `rex_config` VALUES 
  ('be_style/customizer','codemirror','1'),
  ('be_style/customizer','codemirror_theme','\"eclipse\"'),
  ('be_style/customizer','labelcolor','\"#43a047\"'),
  ('be_style/customizer','showlink','1'),
  ('core','package-config','{\"backup\":{\"install\":true,\"status\":true},\"be_style\":{\"install\":true,\"status\":true,\"plugins\":{\"customizer\":{\"install\":true,\"status\":true},\"redaxo\":{\"install\":true,\"status\":true}}},\"cronjob\":{\"install\":false,\"status\":false,\"plugins\":{\"article_status\":{\"install\":false,\"status\":false},\"optimize_tables\":{\"install\":false,\"status\":false}}},\"install\":{\"install\":true,\"status\":true},\"media_manager\":{\"install\":true,\"status\":true},\"mediapool\":{\"install\":true,\"status\":true},\"metainfo\":{\"install\":true,\"status\":true},\"phpmailer\":{\"install\":true,\"status\":true},\"project\":{\"install\":true,\"status\":true},\"quick_navigation\":{\"install\":true,\"status\":true},\"redactor2\":{\"install\":true,\"status\":true},\"structure\":{\"install\":true,\"status\":true,\"plugins\":{\"content\":{\"install\":true,\"status\":true},\"history\":{\"install\":false,\"status\":false},\"version\":{\"install\":false,\"status\":false}}},\"uploader\":{\"install\":true,\"status\":true},\"users\":{\"install\":true,\"status\":true},\"yform\":{\"install\":true,\"status\":true,\"plugins\":{\"docs\":{\"install\":true,\"status\":true},\"email\":{\"install\":true,\"status\":true},\"manager\":{\"install\":true,\"status\":true},\"tools\":{\"install\":false,\"status\":false}}},\"yrewrite\":{\"install\":true,\"status\":true}}'),
  ('core','package-order','[\"be_style\",\"be_style\\/customizer\",\"be_style\\/redaxo\",\"users\",\"backup\",\"install\",\"media_manager\",\"mediapool\",\"phpmailer\",\"project\",\"redactor2\",\"structure\",\"uploader\",\"metainfo\",\"quick_navigation\",\"structure\\/content\",\"yform\",\"yform\\/docs\",\"yform\\/email\",\"yform\\/manager\",\"yrewrite\"]'),
  ('core','version','\"5.6.2\"'),
  ('media_manager','interlace','[\"jpg\"]'),
  ('media_manager','jpg_quality','85'),
  ('media_manager','png_compression','5'),
  ('media_manager','webp_quality','85'),
  ('phpmailer','bcc','\"\"'),
  ('phpmailer','charset','\"utf-8\"'),
  ('phpmailer','confirmto','\"\"'),
  ('phpmailer','encoding','\"8bit\"'),
  ('phpmailer','from','\"\"'),
  ('phpmailer','fromname','\"Mailer\"'),
  ('phpmailer','host','\"localhost\"'),
  ('phpmailer','log','1'),
  ('phpmailer','mailer','\"mail\"'),
  ('phpmailer','password','\"\"'),
  ('phpmailer','port','25'),
  ('phpmailer','priority','0'),
  ('phpmailer','smtpauth','false'),
  ('phpmailer','smtpsecure','\"\"'),
  ('phpmailer','smtp_debug','\"0\"'),
  ('phpmailer','test_address','\"\"'),
  ('phpmailer','username','\"\"'),
  ('phpmailer','wordwrap','120'),
  ('quick_navigation','quicknavi_favs1','[]'),
  ('structure','notfound_article_id','19'),
  ('structure','start_article_id','1'),
  ('structure/content','default_template_id','1'),
  ('uploader','image-max-filesize','30'),
  ('uploader','image-max-height','4000'),
  ('uploader','image-max-width','4000'),
  ('uploader','image-resize-checked','true');
/*!40000 ALTER TABLE `rex_config` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_media`;
CREATE TABLE `rex_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `attributes` text,
  `filetype` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `originalname` varchar(255) DEFAULT NULL,
  `filesize` varchar(255) DEFAULT NULL,
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_media` WRITE;
/*!40000 ALTER TABLE `rex_media` DISABLE KEYS */;
INSERT INTO `rex_media` VALUES 
  (1,1,'','image/jpeg','01.jpg','01.jpg','19383',236,160,'Filtro de aire anticongelante','2018-02-28 17:56:55','2018-03-16 09:09:26','admin','admin',0),
  (2,1,'','image/jpeg','02.jpg','02.jpg','17669',236,160,'Filtro de aceite','2018-02-28 20:28:25','2018-02-28 20:28:25','admin','admin',0),
  (3,1,'','image/jpeg','1.jpg','1.jpg','64346',962,520,'Tecfil','2018-02-28 20:49:10','2018-03-31 12:27:45','admin','admin',0),
  (6,2,'','image/jpeg','2.jpg','2.jpg','204703',1920,816,'Unifilter','2018-02-28 21:28:09','2018-03-23 12:26:49','admin','admin',0),
  (9,3,'','image/jpeg','keltec.jpg','Keltec.jpg','51366',1056,500,'Keltec','2018-02-28 22:00:37','2018-02-28 22:00:37','admin','admin',0),
  (10,4,'','image/jpeg','racor.jpg','racor.jpg','36878',675,368,'Parker - Racor','2018-03-01 16:59:53','2018-03-22 11:16:06','admin','admin',0),
  (11,4,'','image/png','01.png','01.png','293187',800,533,'Filtro de Combustible 900FH Racor / Separador de Agua 30 micras','2018-03-01 17:06:12','2018-03-01 17:06:12','admin','admin',0),
  (12,3,'','image/jpeg','keltec2.jpg','Keltec2.jpg','26844',630,335,'Keltec','2018-03-01 17:39:11','2018-03-23 12:47:32','admin','admin',0),
  (13,5,'','image/jpeg','baldwin.jpg','baldwin.jpg','85132',1030,520,'Baldwin','2018-03-01 17:48:06','2018-03-01 17:48:06','admin','admin',0),
  (14,0,'','image/png','multifiltros-logo.png','multifiltros-logo.png','51552',281,142,'Multifiltros Santa Cruz S.R.L.','2018-03-02 12:55:19','2018-03-02 12:55:19','admin','admin',0),
  (15,2,'','image/jpeg','001.jpg','001.jpg','116717',660,390,'Camiones y autobuses','2018-03-15 12:27:50','2018-03-15 12:27:50','admin','admin',0),
  (16,2,'','image/jpeg','002.jpg','002.jpg','706230',660,381,'Vehículos Utilitarios','2018-03-15 12:31:17','2018-03-15 12:31:17','admin','admin',0),
  (17,2,'','image/jpeg','003.jpg','003.jpg','276242',656,397,'Máquinas Agrícolas','2018-03-15 12:36:47','2018-03-15 12:36:47','admin','admin',0),
  (18,2,'','image/jpeg','004.jpg','004.jpg','228929',520,296,'Construcción civil','2018-03-15 12:41:56','2018-03-15 12:44:11','admin','admin',0),
  (19,2,'','image/jpeg','006.jpg','006.jpg','785533',520,278,'Cabinas de Pintura','2018-03-15 12:41:56','2018-03-15 12:46:16','admin','admin',0),
  (20,2,'','image/jpeg','005.jpg','005.jpg','179778',520,259,'Generación de energía','2018-03-15 12:41:56','2018-03-15 12:44:41','admin','admin',0),
  (21,2,'','image/jpeg','008.jpg','008.jpg','887917',520,309,'Amoníaco','2018-03-15 12:41:56','2018-03-15 12:46:42','admin','admin',0),
  (22,2,'','image/jpeg','009.jpg','009.jpg','244473',600,363,'Especial','2018-03-15 12:41:56','2018-03-15 12:47:28','admin','admin',0),
  (23,2,'','image/jpeg','007.jpg','007.jpg','137233',520,244,'EDM','2018-03-15 12:41:56','2018-03-15 12:46:28','admin','admin',0),
  (24,2,'','image/png','multifiltros-logo_1.png','multifiltros-logo.png','51552',281,142,'Carretillas elevadoras','2018-03-15 12:45:37','2018-03-15 12:45:44','admin','admin',0),
  (25,1,'','image/jpeg','03.jpg','03.jpg','16468',236,160,'Filtro de combustible','2018-03-16 09:18:59','2018-03-16 09:18:59','admin','admin',0),
  (26,1,'','image/jpeg','04.jpg','04.jpg','8845',236,160,'Filtro de Aire Acondicionado / Cabina del timón','2018-03-16 09:20:50','2018-03-16 09:20:50','admin','admin',0),
  (27,1,'','image/jpeg','05.jpg','05.jpg','13729',236,160,'Filtros ecológicos','2018-03-16 09:21:25','2018-03-16 09:21:25','admin','admin',0),
  (28,1,'','image/jpeg','06.jpg','06.jpg','16650',236,160,'Separadores Filtros','2018-03-16 09:22:01','2018-03-16 09:22:01','admin','admin',0),
  (29,1,'','image/jpeg','07.jpg','07.jpg','5117',236,160,'Deshumidificadores Filtros','2018-03-16 09:22:56','2018-03-16 09:22:56','admin','admin',0),
  (30,1,'','image/jpeg','08.jpg','08.jpg','22910',236,160,'Filtros TECMAX','2018-03-16 09:23:37','2018-03-16 09:23:37','admin','admin',0),
  (39,7,'','image/jpeg','01_2.jpg','01_2.jpg','59332',1000,1000,'Filtro De Combustible Serie De Turbina Del Separador De Agua','2018-03-16 16:00:27','2018-03-16 16:01:07','admin','admin',0),
  (40,7,'','image/jpeg','02_3.jpg','02_3.jpg','315227',1000,1000,'Filtro De Combustible Desechable SNAPP ™ / Separador De Agua - Racor','2018-03-16 16:00:27','2018-03-16 16:01:39','admin','admin',0),
  (41,7,'','image/jpeg','04_2.jpg','04_2.jpg','288912',1000,1000,'Filtración De Transferencia De Combustible FBO - Racor','2018-03-16 16:00:27','2018-03-16 16:02:37','admin','admin',0),
  (42,7,'','image/jpeg','06_2.jpg','06_2.jpg','70085',1000,1000,'Sistemas De Bomba De Filtro De Cartucho','2018-03-16 16:00:27','2018-03-16 16:03:38','admin','admin',0),
  (43,7,'','image/jpeg','03_2.jpg','03_2.jpg','324644',1000,1000,'Filtro De Combustible Diesel Spin-On','2018-03-16 16:00:27','2018-03-16 16:02:13','admin','admin',0),
  (44,7,'','image/jpeg','05_2.jpg','05_2.jpg','323951',1000,1000,'Filtro De Combustible GreenMax / Separador De Agua','2018-03-16 16:00:28','2018-03-16 16:03:09','admin','admin',0),
  (45,7,'','image/jpeg','07_2.jpg','07_2.jpg','436020',1000,1000,'Filtros Y Prefiltros De Combustible','2018-03-16 16:00:28','2018-03-16 16:04:01','admin','admin',0),
  (46,7,'','image/jpeg','08_2.jpg','08_2.jpg','74568',1000,1000,'Separador De Agua De Filtros De Combustible De Alta Presión','2018-03-16 16:00:28','2018-03-16 16:06:02','admin','admin',0),
  (47,7,'','image/jpeg','09.jpg','09.jpg','343706',1000,1000,'Sistema De Pulido De Combustible De Bomba Y Filtro','2018-03-16 16:00:28','2018-03-16 16:06:31','admin','admin',0),
  (48,7,'','image/jpeg','10.jpg','10.jpg','369584',1000,1000,'Spin-On Filter Pump Systems','2018-03-16 16:00:28','2018-03-16 16:07:01','admin','admin',0),
  (49,2,'','image/jpeg','slide.jpg','slide.jpg','51935',660,342,'Unifilter','2018-03-23 12:27:41','2018-08-23 17:37:39','admin','admin',0),
  (50,3,'','image/jpeg','29511764_211285709605679_4250097630924767232_n.jpg','29511764_211285709605679_4250097630924767232_n.jpg','42741',960,276,'Keltec','2018-03-28 12:55:34','2018-03-28 12:55:34','admin','admin',0),
  (51,8,'','image/jpeg','racor01.jpg','racor01.jpg','77278',1000,1000,'Filtro Spin-On En Línea De Baja Presión - Serie Maxiflow​','2018-03-29 10:44:15','2018-03-29 10:44:15','admin','admin',0),
  (52,8,'','image/jpeg','racor02.jpg','racor02.jpg','52274',1000,1000,'Filtro de baja presión, serie 12AT / 50AT','2018-03-29 10:53:10','2018-03-29 10:53:10','admin','admin',0),
  (53,8,'','image/jpeg','racor03.jpg','racor03.jpg','117254',1108,850,'Filtro De Combustible','2018-03-29 10:55:49','2018-03-29 10:58:58','admin','admin',0),
  (54,5,'','image/jpeg','balwin01.jpg','balwin01.jpg','14476',280,280,'PA31010 - Elemento de aire primario EnduraPanel ™','2018-03-29 11:09:29','2018-03-29 11:09:29','admin','admin',0),
  (55,5,'','image/jpeg','balwin02.jpg','balwin02.jpg','13517',318,310,'Filtros de Combustible Baldwin Original BF7682-D, BF7681-D, BF7674-D','2018-03-29 11:14:29','2018-03-29 11:15:45','admin','admin',0),
  (56,5,'','image/jpeg','balwin03.jpg','balwin03.jpg','43096',700,535,'Filtros para aceite hidráulico','2018-03-29 11:17:45','2018-03-29 11:18:10','admin','admin',0),
  (57,9,'','image/jpeg','mc-repuesto-banner.jpg','mc-repuesto-banner.jpg','45712',980,354,'MC Repuestos','2018-03-29 11:49:17','2018-03-29 11:52:03','admin','admin',0),
  (58,9,'','image/jpeg','mc-repuesto01.jpg','mc-repuesto01.jpg','10576',294,297,'​FCD-311 DUOFILTRO DE COMBUSTIBLE','2018-03-29 11:56:21','2018-03-29 11:56:21','admin','admin',0),
  (59,9,'','image/jpeg','mc-repuesto02.jpg','mc-repuesto02.jpg','12371',316,289,'​FCD-312 DUOFILTRO DE COMBUSTIBLE','2018-03-29 11:57:50','2018-03-29 11:57:50','admin','admin',0),
  (60,9,'','image/jpeg','mc-repuesto03.jpg','mc-repuesto03.jpg','18508',362,354,'​FCD-301 DUOFILTRO PERKINS','2018-03-29 11:59:11','2018-03-29 11:59:11','admin','admin',0),
  (61,9,'','image/jpeg','mc-repuesto04.jpg','mc-repuesto04.jpg','7817',229,205,'​VV-02 VASO TIPO CAV PARA FILTROS','2018-03-29 12:00:50','2018-03-29 12:00:50','admin','admin',0),
  (62,10,'','image/jpeg','tbfil01.jpg','tbfil01.jpg','49926',968,502,'Tb Fil','2018-03-29 12:03:13','2018-08-10 17:23:49','admin','admin',0),
  (63,3,'','image/jpeg','keltec01.jpg','keltec01.jpg','30320',600,714,'Filtro coalescente','2018-03-29 12:25:38','2018-03-29 12:25:49','admin','admin',0),
  (64,3,'','image/jpeg','keltec03.jpg','keltec03.jpg','32535',475,480,'	 Filtros de aire comprimido','2018-03-29 12:26:53','2018-03-29 12:27:01','admin','admin',0),
  (65,3,'','image/jpeg','keltec04.jpg','keltec04.jpg','78598',800,815,'Filtros de aire','2018-03-29 12:36:21','2018-03-29 12:38:08','admin','admin',0),
  (66,3,'','image/jpeg','keltec02.jpg','keltec02.jpg','14918',556,356,'Filtro Separador Ar/Óleo Similar Kaeser SM7,5, SM10, SM15, N15','2018-03-29 12:38:36','2018-03-29 12:39:06','admin','admin',0),
  (67,11,'','image/jpeg','unifilter01.jpg','unifilter01.jpg','50451',656,396,'','2018-03-29 17:58:56','2018-03-29 17:58:56','admin','admin',0),
  (73,11,'','image/jpeg','la07.jpg','la07.jpg','25293',317,325,'Filtro Separador Ar/Óleo - FSKA21','2018-03-31 10:59:30','2018-03-31 11:15:11','admin','admin',0),
  (74,11,'','image/jpeg','la04.jpg','la04.jpg','37625',317,365,'Filtro de aire primario - UARS8043P','2018-03-31 10:59:30','2018-03-31 11:14:07','admin','admin',0),
  (75,11,'','image/jpeg','la02.jpg','la02.jpg','33209',317,340,'Filtro hidráulico - UHO79GUS260','2018-03-31 10:59:30','2018-03-31 11:13:13','admin','admin',0),
  (76,11,'','image/jpeg','la06.jpg','la06.jpg','21262',317,315,'Elemento separador de aire/óleo - FSA37KIT','2018-03-31 10:59:30','2018-03-31 11:09:35','admin','admin',0),
  (77,11,'','image/jpeg','la05.jpg','la05.jpg','25937',317,315,'Elemento filtrante de aire - UARS469601P','2018-03-31 10:59:30','2018-03-31 11:08:02','admin','admin',0),
  (78,11,'','image/jpeg','la01.jpg','la01.jpg','30013',317,307,'Elemento filtrante de combustible - UH100LBN235','2018-03-31 10:59:30','2018-03-31 11:04:41','admin','admin',0),
  (79,11,'','image/jpeg','la03.jpg','la03.jpg','37947',317,365,'Filtro de aire secundario - UARS8044F','2018-03-31 10:59:30','2018-03-31 11:02:28','admin','admin',0),
  (80,1,'','image/jpeg','banner-65a_os.jpg','banner-65años.jpg','71365',962,520,'Tecfil','2018-03-31 12:28:04','2018-03-31 12:28:04','admin','admin',0),
  (81,12,'','image/jpeg','poppup01.jpg','poppup01.jpg','197535',900,420,'Parker Racor','2018-08-13 12:16:07','2018-08-13 12:16:07','admin','admin',0),
  (82,12,'','image/jpeg','poppup03.jpg','poppup03.jpg','111369',900,420,'Parker Racor','2018-08-13 12:16:07','2018-08-13 12:16:07','admin','admin',0),
  (83,12,'','image/jpeg','poppup04.jpg','poppup04.jpg','118008',900,420,'Parker Racor','2018-08-13 12:16:07','2018-08-13 12:16:07','admin','admin',0),
  (84,12,'','image/jpeg','poppup02.jpg','poppup02.jpg','211155',900,420,'Parker Racor','2018-08-13 12:16:07','2018-08-13 12:16:07','admin','admin',0),
  (85,13,'','image/jpeg','tecfil.jpg','tecfil.jpg','49452',962,440,'Banner','2018-08-23 16:46:20','2018-08-23 16:53:58','admin','admin',0),
  (86,13,'','image/jpeg','unifilter.jpg','unifilter.jpg','75287',962,440,'unifilter','2018-08-23 17:38:21','2018-08-24 11:19:24','admin','admin',0),
  (87,13,'','image/jpeg','parker-racor.jpg','parker-racor.jpg','60379',962,440,'Parker racor','2018-08-23 17:45:26','2018-08-24 11:19:55','admin','admin',0),
  (88,13,'','image/jpeg','keltec_1.jpg','keltec_1.jpg','48398',962,440,'keltec','2018-08-24 10:24:03','2018-08-24 11:19:41','admin','admin',0),
  (89,13,'','image/jpeg','baldwin_1.jpg','baldwin_1.jpg','60754',962,440,'Baldwin','2018-08-24 10:28:30','2018-08-24 10:28:30','admin','admin',0),
  (90,13,'','image/jpeg','mc-repuestos.jpg','mc-repuestos.jpg','43797',962,440,'MC Repuestos','2018-08-24 10:59:28','2018-08-24 11:04:58','admin','admin',0),
  (91,13,'','image/jpeg','tbfil.jpg','tbfil.jpg','44407',962,440,'','2018-08-24 11:12:30','2018-08-24 11:12:30','admin','admin',0),
  (92,14,'','image/jpeg','bfbrand1014.jpg','bfbrand1014.jpg','36232',771,378,'BALDWIN','2018-08-24 12:43:40','2018-08-25 12:42:55','admin','soporte-multifiltros',0),
  (93,14,'','image/png','keltec-technolab-2.png','keltec-technolab-2.png','17139',320,250,'KELTEC','2018-08-24 12:43:40','2018-08-25 12:42:24','admin','soporte-multifiltros',0),
  (94,14,'','image/jpeg','rk60hp-filtro-de-combustible-separador-de-agua-rama-d_nq_np_986223-mla26530792956_122017-f.jpg','rk60hp-filtro-de-combustible-separador-de-agua-rama-d_nq_np_986223-mla26530792956_122017-f.jpg','118346',1190,1200,'logo','2018-08-24 12:43:40','2018-08-24 12:43:40','admin','admin',0),
  (95,14,'','image/jpeg','logo.jpg','logo.jpg','6271',198,105,'TBFIL','2018-08-24 12:43:40','2018-08-25 12:42:39','admin','soporte-multifiltros',0),
  (96,14,'','image/png','images.png','images.png','6665',400,120,'UNIFILTER','2018-08-24 12:43:40','2018-08-25 12:44:39','admin','soporte-multifiltros',0),
  (97,14,'','image/png','logo-tecfil.png','logo-tecfil.png','6984',600,324,'TECFIL','2018-08-24 16:41:13','2018-08-25 12:44:22','admin','soporte-multifiltros',0),
  (98,14,'','image/jpeg','parker-racor-logo_1.jpg','parker-racor-logo_1.jpg','4773',300,130,'PARKER RACOR','2018-08-24 16:49:31','2018-08-25 12:43:41','admin','soporte-multifiltros',0),
  (99,14,'','image/jpeg','mc.jpg','mc.jpg','11069',300,300,'MC REPUESTOS','2018-08-24 16:55:21','2018-08-25 12:43:22','admin','soporte-multifiltros',0),
  (100,0,'','image/jpeg','nosotros.jpg','nosotros.jpg','67272',1000,278,'Multifiltros','2018-08-25 11:19:24','2018-08-27 09:39:36','soporte-multifiltros','admin',0),
  (101,0,'','image/jpeg','rama.jpg','rama.jpg','41574',650,426,'RAMA','2018-08-27 08:47:06','2018-08-27 08:47:06','admin','admin',0),
  (102,15,'','image/jpeg','slide-rama.jpg','slide-rama.jpg','25727',675,368,'Rama','2018-08-27 09:40:51','2018-08-27 09:40:51','admin','admin',0),
  (103,15,'','image/jpeg','banner-rama.jpg','banner-rama.jpg','31214',906,313,'Rama','2018-08-27 09:46:25','2018-08-27 09:50:26','admin','admin',0),
  (104,15,'','image/jpeg','rama01.jpg','rama01.jpg','6634',260,370,'Rama 190RK','2018-08-27 17:50:50','2018-08-27 17:53:43','admin','admin',0),
  (105,15,'','image/jpeg','rama03.jpg','rama03.jpg','7096',260,370,'Rama 150RK','2018-08-27 17:50:50','2018-08-27 17:53:23','admin','admin',0),
  (106,15,'','image/jpeg','rama04.jpg','rama04.jpg','7431',260,370,'Rama 180RK','2018-08-27 17:50:50','2018-08-27 17:52:55','admin','admin',0),
  (107,15,'','image/jpeg','rama02.jpg','rama02.jpg','7129',260,370,'Rama 160 RK','2018-08-27 17:50:50','2018-08-27 17:50:50','admin','admin',0);
/*!40000 ALTER TABLE `rex_media` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_media_category`;
CREATE TABLE `rex_media_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `path` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `attributes` text,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_media_category` WRITE;
/*!40000 ALTER TABLE `rex_media_category` DISABLE KEYS */;
INSERT INTO `rex_media_category` VALUES 
  (1,'Tecfil',0,'|','2018-02-28 17:56:14','2018-02-28 17:56:14','admin','admin','',0),
  (2,'Unifilter',0,'|','2018-02-28 21:07:27','2018-02-28 21:07:27','admin','admin','',0),
  (3,'Keltec',0,'|','2018-02-28 21:44:37','2018-02-28 21:44:37','admin','admin','',0),
  (4,'Racor',0,'|','2018-03-01 16:58:52','2018-03-01 16:58:52','admin','admin','',0),
  (5,'Baldwin',0,'|','2018-03-01 17:47:48','2018-03-01 17:47:48','admin','admin','',0),
  (7,'Filtración de combustible de motores',4,'|4|','2018-03-16 16:00:14','2018-03-16 16:00:14','admin','admin','',0),
  (8,'FILTROS, VARIOS',4,'|4|','2018-03-29 10:43:19','2018-03-29 10:52:12','admin','admin','',0),
  (9,'Mc-Repuestos',0,'|','2018-03-29 11:48:53','2018-03-29 11:48:53','admin','admin','',0),
  (10,'Tb fil',0,'|','2018-03-29 12:02:48','2018-03-29 12:02:48','admin','admin','',0),
  (11,'Lanzamientos',2,'|2|','2018-03-29 17:58:43','2018-03-29 17:58:43','admin','admin','',0),
  (12,'Poppup imágenes',0,'|','2018-08-13 12:08:57','2018-08-13 12:08:57','admin','admin','',0),
  (13,'Banners para interiores',0,'|','2018-08-23 16:45:22','2018-08-23 16:45:22','admin','admin','',0),
  (14,'Logos-Marcas',0,'|','2018-08-24 12:42:59','2018-08-24 12:42:59','admin','admin','',0),
  (15,'Rama',0,'|','2018-08-27 09:40:27','2018-08-27 09:40:27','admin','admin','',0);
/*!40000 ALTER TABLE `rex_media_category` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_media_manager_type`;
CREATE TABLE `rex_media_manager_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_media_manager_type` WRITE;
/*!40000 ALTER TABLE `rex_media_manager_type` DISABLE KEYS */;
INSERT INTO `rex_media_manager_type` VALUES 
  (1,1,'rex_mediapool_detail','Zur Darstellung von Bildern in der Detailansicht im Medienpool'),
  (2,1,'rex_mediapool_maximized','Zur Darstellung von Bildern im Medienpool wenn maximiert'),
  (3,1,'rex_mediapool_preview','Zur Darstellung der Vorschaubilder im Medienpool'),
  (4,1,'rex_mediabutton_preview','Zur Darstellung der Vorschaubilder in REX_MEDIA_BUTTON[]s'),
  (5,1,'rex_medialistbutton_preview','Zur Darstellung der Vorschaubilder in REX_MEDIALIST_BUTTON[]s'),
  (6,0,'multifiltros-thumb','Imagen de 300px de altura'),
  (7,0,'multifiltros-full','Imagen de 968px de anchura'),
  (8,0,'multifiltros-thumb2','300px de ancho');
/*!40000 ALTER TABLE `rex_media_manager_type` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_media_manager_type_effect`;
CREATE TABLE `rex_media_manager_type_effect` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL,
  `effect` varchar(255) NOT NULL,
  `parameters` text NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `updatedate` datetime NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `createuser` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_media_manager_type_effect` WRITE;
/*!40000 ALTER TABLE `rex_media_manager_type_effect` DISABLE KEYS */;
INSERT INTO `rex_media_manager_type_effect` VALUES 
  (1,1,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"200\",\"rex_effect_resize_height\":\"200\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (2,2,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"600\",\"rex_effect_resize_height\":\"600\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (3,3,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"80\",\"rex_effect_resize_height\":\"80\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (4,4,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"246\",\"rex_effect_resize_height\":\"246\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (5,5,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"246\",\"rex_effect_resize_height\":\"246\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (6,6,'resize','{\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"\",\"rex_effect_resize_height\":\"300\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"enlarge\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'2018-02-28 21:13:20','admin','2018-02-28 21:13:20','admin'),
  (7,7,'resize','{\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"968\",\"rex_effect_resize_height\":\"\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"enlarge\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'2018-02-28 21:15:01','admin','2018-02-28 21:15:01','admin'),
  (8,8,'resize','{\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"300\",\"rex_effect_resize_height\":\"\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"}}',1,'2018-08-24 16:44:04','admin','2018-08-24 16:44:04','admin');
/*!40000 ALTER TABLE `rex_media_manager_type_effect` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_metainfo_field`;
CREATE TABLE `rex_metainfo_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `priority` int(10) unsigned NOT NULL,
  `attributes` text NOT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `default` varchar(255) NOT NULL,
  `params` text,
  `validate` text,
  `callback` text,
  `restrictions` text,
  `createuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `updatedate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_metainfo_type`;
CREATE TABLE `rex_metainfo_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `dbtype` varchar(255) NOT NULL,
  `dblength` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_metainfo_type` WRITE;
/*!40000 ALTER TABLE `rex_metainfo_type` DISABLE KEYS */;
INSERT INTO `rex_metainfo_type` VALUES 
  (1,'text','text',0),
  (2,'textarea','text',0),
  (3,'select','varchar',255),
  (4,'radio','varchar',255),
  (5,'checkbox','varchar',255),
  (6,'REX_MEDIA_WIDGET','varchar',255),
  (7,'REX_MEDIALIST_WIDGET','text',0),
  (8,'REX_LINK_WIDGET','varchar',255),
  (9,'REX_LINKLIST_WIDGET','text',0),
  (10,'date','text',0),
  (11,'datetime','text',0),
  (12,'legend','text',0),
  (13,'time','text',0);
/*!40000 ALTER TABLE `rex_metainfo_type` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_module`;
CREATE TABLE `rex_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `output` mediumtext NOT NULL,
  `input` mediumtext NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `attributes` text,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_module` WRITE;
/*!40000 ALTER TABLE `rex_module` DISABLE KEYS */;
INSERT INTO `rex_module` VALUES 
  (1,'Vertical Links Menu','<?php\r\n	if (\'REX_LINKLIST[1]\' != \'\') {\r\n		$linklist = explode(\',\',\'REX_LINKLIST[1]\');\r\n                $enfasis = 0;\r\n		foreach ($linklist as $link) {\r\n			$art = rex_article::get($link);\r\n			if ($art instanceof rex_article) {\r\n                           $art_name = $art->getValue(\'name\');\r\n			  if($enfasis == 0){\r\necho \'<li class=\"enfasis\"><a href=\"\'.rex_getUrl($link).\'\"><span>\'.$art_name.\'</span></a></li>\';\r\n$enfasis = 1;                           \r\n                           }else{\r\necho \'<li><a href=\"\'.rex_getUrl($link).\'\"><span>\'.$art_name.\'</span></a></li>\';                           \r\n                           }	\r\n\r\n			}\r\n		}\r\n	}\r\n	?>','<!-- *******************************************************\r\nASIDE LINK MENU\r\n******************************************************** -->\r\n<fieldset class=\"form-horizontal\">\r\n	<div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Menu Links: </label>\r\n        <div class=\"col-sm-10\">\r\n            REX_LINKLIST[id=\"1\" widget=\"1\"]\r\n        </div>\r\n    </div>\r\n\r\n</fieldset>','admin','admin','2018-02-28 15:05:48','2018-03-15 16:36:45','',0),
  (2,'02.Editor de Texto','<div class=\"main-main__content l-90 s-100 to-center l-block\">\r\n   REX_VALUE[id=1 output=html]\r\n</div>','<textarea class=\"redactorEditor2-basic\" name=\"REX_INPUT_VALUE[1]\">REX_VALUE[1]</textarea>','admin','admin','2018-02-28 16:50:43','2018-02-28 20:25:57','',0),
  (3,'Titulo Banner','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\'); \r\n  /* Resize imagen */\r\n  $mediatype = \'multifiltros-full\';\r\n  if (\"REX_MEDIA[1]\" != \'\'){\r\n    $imagen = \'index.php?rex_media_type=\' . $mediatype . \'&rex_media_file=REX_MEDIA[1]\';\r\n  }else{\r\n    $imagen = \'https://images.pexels.com/photos/265614/pexels-photo-265614.jpeg?w=1260&amp;h=750&amp;auto=compress&amp;cs=tinysrgb\';\r\n  }\r\n?>\r\n<div class=\"rex-item main-banner--dark main-banner\">\r\n  <div class=\"rex-container\">\r\n    <div class=\"rex-item l-75 to-center\">\r\n      <img class=\"main-banner__img\" src=\"<?= $imagen; ?>\" alt=\"<?php echo rex_article::getCurrent()->getName() ?>\" />\r\n      <div class=\"main-banner__data data--accent\">\r\n        <h1><?php echo rex_article::getCurrent()->getName() ?></h1>\r\n        <div class=\"subtitle\">REX_VALUE[id=1 output=html]</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\');\r\n  ?>\r\n<fieldset>\r\n  <input type=\"hidden\" name=\"save\" value=\"1\">\r\n  <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n  <dl class=\"rex-form-group form-group\">\r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Titulo de banner:</label>\r\n    </dt>\r\n    <dd>\r\n      <p><?php echo rex_article::getCurrent()->getName() ?></p>\r\n    </dd>\r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Imagen del Fondo: </label>\r\n    </dt>\r\n    <dd>REX_MEDIA[id=1 widget=1]</dd>    \r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Descripcion [Opcional]:</label>\r\n    </dt>\r\n    <dd>\r\n      <textarea class=\"redactorEditor2-basic\" name=\"REX_INPUT_VALUE[1]\">REX_VALUE[1]</textarea>\r\n    </dd>\r\n  </dl>\r\n</fieldset>','admin','admin','2018-02-28 17:09:27','2018-03-28 17:04:11','',0),
  (4,'01.Subtitulo de contenido','<h2 class=\"center rex-item l-block\" id=\"REX_VALUE[2]\">REX_VALUE[1]</h2>','<fieldset>\r\n            <input type=\"hidden\" name=\"save\" value=\"1\">\r\n            <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n            <dl class=\"rex-form-group form-group\"><dt><label for=\"rex-id-meta-article-name\">Subtitulo</label></dt><dd><input class=\"form-control\" type=\"text\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[1]\" value=\"REX_VALUE[1]\"></dd></dl>\r\n\r\n<dl class=\"rex-form-group form-group\"><dt><label for=\"rex-id-meta-article-name\">ID (Opcional)</label></dt><dd><input class=\"form-control\" type=\"text\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[2]\" value=\"REX_VALUE[2]\"></dd></dl>\r\n</fieldset>','admin','admin','2018-02-28 17:16:13','2018-03-24 09:38:01','',0),
  (5,'03.Producto','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\'); \r\n  /* Resize imagen */\r\n  $mediatype = \'multifiltros-thumb\';\r\n  if (\"REX_MEDIA[1]\" != \'\'){\r\n    $imagen = \'index.php?rex_media_type=\' . $mediatype . \'&rex_media_file=REX_MEDIA[1]\';\r\n  }else{\r\n    $imagen = \'./assets/images/multifiltros-logo.png\';\r\n  }\r\n?>\r\n<div class=\"l-90 s-100 to-center l-block\">\r\n  <div class=\"card card--mini-horizontal rex-container\">\r\n    <div class=\"card__img l-25\">\r\n      <img src=\"<?= $imagen; ?>\" alt=\"<?php if ($media instanceof rex_media) { echo $media->getTitle();}?>\">\r\n    </div>\r\n    <div class=\"card__content l-75\">\r\n      <div class=\"card__title\"><b><?php if ($media instanceof rex_media) { echo $media->getTitle();}?>:</b></div>\r\n      <div class=\"card__body\">\r\n        REX_VALUE[id=1 output=html]\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\');\r\n?>\r\n<fieldset>\r\n  <input type=\"hidden\" name=\"save\" value=\"1\">\r\n  <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n  <dl class=\"rex-form-group form-group\">\r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Titulo de producto:</label>\r\n    </dt>\r\n    <dd>\r\n      <p><?php if ($media instanceof rex_media) { echo $media->getTitle();}else{echo \'Añadir imagen para ver el titulo\';} ?></p>\r\n    </dd>\r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Imagen del producto: </label>\r\n    </dt>\r\n    <dd>REX_MEDIA[id=1 widget=1]</dd>    \r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Descripcion del producto:</label>\r\n    </dt>\r\n    <dd>\r\n      <textarea class=\"redactorEditor2-basic\" name=\"REX_INPUT_VALUE[1]\">REX_VALUE[1]</textarea>\r\n    </dd>\r\n  </dl>\r\n</fieldset>','admin','admin','2018-02-28 17:19:45','2018-03-02 15:09:57','',0),
  (6,'Fomulario de Contacto','<form class=\"rex-container form\" v-on:submit.prevent=\"submitForm\">\r\n                <div class=\"m-50 s-100 wow fadeInRight\" data-wow-duration=\"1.5s\" data-wow-delay=\"0\">\r\n                  <div class=\"rex-item form__item\">\r\n                    <label class=\"form__label\" for=\"\">Nombre completo: </label>\r\n                    <input class=\"form__input\" v-model=\"vue.nombre\" type=\"text\" required=\"\">\r\n                  </div>\r\n                  <div class=\"rex-item form__item\">\r\n                    <label class=\"form__label\" for=\"\">Teléfono: </label>\r\n                    <input class=\"form__input\" v-model=\"vue.telefono\" type=\"number\">\r\n                  </div>\r\n                  <div class=\"rex-item form__item\">\r\n                    <label class=\"form__label\" for=\"\">Celular: </label>\r\n                    <input class=\"form__input\" v-model=\"vue.movil\" type=\"number\" required=\"\">\r\n                  </div>\r\n                  <div class=\"rex-item form__item\">\r\n                    <label class=\"form__label\" for=\"\">Dirección: </label>\r\n                    <input class=\"form__input\" v-model=\"vue.direccion\" type=\"text\" required=\"\">\r\n                  </div>\r\n                  <div class=\"rex-item form__item\">\r\n                    <label class=\"form__label\" for=\"\">Ciudad: </label>\r\n                    <input class=\"form__input\" v-model=\"vue.ciudad\" type=\"text\" required=\"\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"m-50 s-100 wow fadeInLeft\" data-wow-duration=\"1.5s\" data-wow-delay=\"0\">\r\n                  <div class=\"rex-item form__item\">\r\n                    <label class=\"form__label\" for=\"\">Email: </label>\r\n                    <input class=\"form__input\" v-model=\"vue.email\" type=\"email\" required=\"\">\r\n                  </div>\r\n                  <div class=\"rex-item form__item\">\r\n                    <label class=\"form__label\" for=\"\">Mensaje: </label>\r\n                    <textarea class=\"form__input\" v-model=\"vue.mensaje\" name=\"\" cols=\"30\" rows=\"10\" required=\"\"></textarea>\r\n                  </div>\r\n                  <div class=\"rex-item form__item main-center\">\r\n                    <button class=\"button\" type=\"submit\">Enviar</button>\r\n                    <button class=\"button\" v-on:click=\"clearForm()\">Limpiar</button>\r\n                  </div>\r\n                  <div class=\"statusMessage\" v-if=\"formSubmitted\">\r\n                    <h4 v-text=\"vue.envio\"></h4>\r\n                  </div>\r\n                </div>\r\n              </form>','ENTRADA DE FORMULARIO DE CONTACTO','admin','admin','2018-02-28 22:53:48','2018-03-01 15:24:34','',0),
  (7,'a. Cabecera','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\'); \r\n  /* Resize imagen */\r\n  $mediatype = \'multifiltros-thumb\';\r\n  if (\"REX_MEDIA[1]\" != \'\'){\r\n    $imagen = \'index.php?rex_media_type=\' . $mediatype . \'&rex_media_file=REX_MEDIA[1]\';\r\n  }else{\r\n    $imagen = \'./assets/images/multifiltros-logo.png\';\r\n  }\r\n?>\r\n<header class=\"main-header\">\r\n        <div class=\"main-header__border\"></div>\r\n        <div class=\"rex-container main-header__content\">\r\n          <div class=\"rex-item s-80 l-25 xl-20 cross-center main-center\"><a class=\"wow lightSpeedIn\" data-wow-duration=\"1.2s\" data-wow-delay=\"0s\" href=\"<?= rex_url::base(\'\') ?>\"><img class=\"main-logo\" src=\"<?= $imagen; ?>\" alt=\"<?php if ($media instanceof rex_media) { echo $media->getTitle();}?>\"></a></div>\r\n          <div class=\"rex-item s-20 l-75 xl-80 cross-center main-end header-links\">\r\n            <div class=\"main-menu l-30\">\r\n              <h2 class=\"main-menu__title wow slideInLeft\" data-wow-duration=\"1.3s\" data-wow-delay=\"1.0s\">REX_VALUE[1]</h2>\r\n              <h2 class=\"main-menu__title wow slideInLeft\" data-wow-duration=\"1.3s\" data-wow-delay=\"1.2s\">REX_VALUE[2]</h2>\r\n              <h2 class=\"main-menu__title wow slideInLeft\" data-wow-duration=\"1.3s\" data-wow-delay=\"1.2s\">REX_VALUE[3]</h2>\r\n            </div>\r\n            <div class=\"main-menu main-center l-40 logo-parker\">\r\n              <h2 class=\"center main-menu__title wow slideInRight\" data-wow-duration=\"1.3s\" data-wow-delay=\"1.0s\">Representante #1 de</h2>\r\n              <img class=\"wow slideInRight\" src=\"<?= rex_url::base(\'\') ?>assets/images/logo-parker.png\" alt=\"Parker Racor\" data-wow-duration=\"1.3s\" data-wow-delay=\"1.3s\">\r\n              <h2 class=\"center main-menu__title wow slideInRight\" data-wow-duration=\"1.3s\" data-wow-delay=\"1.6s\">en Bolivia</h2>\r\n            </div>\r\n            <div class=\"main-menu l-30 cross-center main-end rex-item\">       \r\n              <nav id=\"main-menu\">\r\n                <ul>\r\n                  REX_ARTICLE[18]         \r\n                </ul>\r\n                REX_ARTICLE[17]\r\n              </nav>\r\n            </div>\r\n            <a class=\"main-menu-toggle to-l\" href=\"javascript:void(0)\" @click=\"menuToggle()\"></a>\r\n          </div>\r\n        </div>\r\n      </header>','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\');\r\n?>\r\n<fieldset>\r\n  <input type=\"hidden\" name=\"save\" value=\"1\">\r\n  <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Logotipo de cabecera: </label>\r\n    </dt>\r\n    <dd>REX_MEDIA[id=1 widget=1]</dd>    \r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Slogan de Cabecera:</label>\r\n    </dt>\r\n    <dd>\r\n      <input class=\"form-control\" type=\"text\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[1]\" value=\"REX_VALUE[1]\">\r\n    </dd>\r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">\r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\"></label>\r\n    </dt>\r\n    <dd>\r\n      <input class=\"form-control\" type=\"text\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[2]\" value=\"REX_VALUE[2]\">\r\n    </dd>\r\n  </dl>\r\n  \r\n  <dl class=\"rex-form-group form-group\">\r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\"></label>\r\n    </dt>\r\n    <dd>\r\n      <input class=\"form-control\" type=\"text\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[3]\" value=\"REX_VALUE[3]\">\r\n    </dd>\r\n  </dl>\r\n\r\n<!-- *******************************************************\r\n    HEADER LINK MENU\r\n******************************************************** -->\r\n   <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Menu Links: </label>\r\n        <div class=\"col-sm-10\">\r\n            REX_LINKLIST[id=\"1\" widget=\"1\"]\r\n        </div>\r\n    </div>\r\n</fieldset>','admin','admin','2018-02-28 23:00:25','2018-08-11 09:33:22','',0),
  (8,'Slide','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\'); \r\n  /* Resize imagen */\r\n  $mediatype = \'multifiltros-full\';\r\n  if (\"REX_MEDIA[1]\" != \'\'){\r\n    $imagen = \'index.php?rex_media_type=\' . $mediatype . \'&rex_media_file=REX_MEDIA[1]\';\r\n  }else{\r\n    $imagen = \'https://images.pexels.com/photos/265614/pexels-photo-265614.jpeg?w=1260&amp;h=750&amp;auto=compress&amp;cs=tinysrgb\';\r\n  }\r\n?>\r\n<div class=\"main-banner-- main-banner\">\r\n   <div class=\"rex-container\">\r\n       <div class=\"rex-item l-75 to-center\">\r\n          <img class=\"main-banner__img\" src=\"<?= $imagen; ?>\" alt=\"<?php if ($media instanceof rex_media) { echo $media->getTitle();}?>\">\r\n          <div class=\"main-banner__data\">\r\n             <h1><span class=\"main-banner__title\"><span><?php if ($media instanceof rex_media) { echo $media->getTitle();}?></span></span></h1>\r\n             <a class=\"button center\" href=\"REX_LINK[id=1 output=url]\">Ver más</a>\r\n          </div>\r\n       </div>\r\n   </div>\r\n</div>','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\');\r\n  ?>\r\n<fieldset>\r\n  <input type=\"hidden\" name=\"save\" value=\"1\">\r\n  <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n  <dl class=\"rex-form-group form-group\">\r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Titulo de banner:</label>\r\n    </dt>\r\n    <dd>\r\n      <p><?php if ($media instanceof rex_media) { echo $media->getTitle();} ?></p>\r\n    </dd>\r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Imagen de Fondo: </label>\r\n    </dt>\r\n    <dd>REX_MEDIA[id=1 widget=1]</dd>    \r\n  </dl>\r\n  <div class=\"form-group\">\r\n     <label class=\"col-sm-2 control-label\">Ver más Link: </label>\r\n     <div class=\"col-sm-10\">\r\n        REX_LINK[id=1 widget=1]\r\n     </div>\r\n  </div>\r\n</fieldset>','admin','admin','2018-03-01 15:58:33','2018-03-01 16:34:27','',0),
  (9,'Mapa Google','<!-- <iframe class=\"map-contact\" src=\"https://www.google.com/maps/d/embed?mid=15oulrLxB6lFTup3pwqPRADfwyogfqpeb\" frameborder=\"0\" width=\"100%\" height=\"480\"></iframe> -->\r\n<div class=\"map-contact\" id=\"map\"></div>\r\n              <script>\r\n                var map;\r\n                function initMap() {\r\n                	var multifiltros = {lat: -17.7634495, lng: -63.1499998};\r\n                	var map = new google.maps.Map(document.getElementById(\'map\'), {\r\n                		scaleControl: true,\r\n                		center: multifiltros,\r\n                		zoom: 16\r\n                	});\r\n                	var iconBase = \'/assets/images/\';\r\n                	var icons = {\r\n                		icon: iconBase + \'marker.png\'\r\n                	};\r\n                	var infowindow = new google.maps.InfoWindow({\r\n                			content:  \'<div id=\"content\">\'+\r\n                									\'<h2 id=\"firstHeading\" class=\"firstHeading\">Multi Filtros Santa cruz - Bolivia</h2>\'+\r\n                									\'<div id=\"bodyContent\">\'+\r\n                										\'<p><b>Descripción:</b> Filtros para autos y vehiculos de las marcas Tecfil, Unifilter, Racor, Keltec, Baldwin, Rama. Lubricantes, refrigerantes, siliconas, bombas de lubricación. Procesador de conbustible especial para la inyección electrónica.</p>\'+\r\n                										\'<p><b>Dirección:</b> Av. Nicolás Suárez #1740, 4to Anillo esq. Diego de Trejo, Parque Industrial.\'+\r\n                										\'. <br>\'+\r\n                										\'<b>Telfs:</b> (591-3) 3493339 <br>\'+\'<b>Cel: </b> <a href=\"https://api.whatsapp.com/send?phone=59174174477\" target=\"_blank\" class=\"whatsapp\">74 17 44 77</a></p>\'+\r\n                									\'</div><hr>\'+\r\n                									\'<footer><a class=\"button--cta center\" href=\"https://www.google.com.bo/maps/place/Tecfil+Filtros/@-17.7631534,-63.151058,18z/data=!4m12!1m6!3m5!1s0x93f1e62e9f651cad:0x2612cf6053c85840!2sTecfil+Filtros!8m2!3d-17.7633415!4d-63.1499562!3m4!1s0x93f1e62e9f651cad:0x2612cf6053c85840!8m2!3d-17.7633415!4d-63.1499562?hl=es\" target=\"_blank\">Ver en google maps</footer>\'+\r\n                								\'</div>\'\r\n                	});\r\n                	infowindow.open(map);\r\n                \r\n                	var marker = new google.maps.Marker({\r\n                		map: map,\r\n                		icon: icons.icon,\r\n                		position: multifiltros\r\n                	});\r\n                	marker.addListener(\'click\', function() {\r\n                		infowindow.open(map, marker);\r\n                	});\r\n                }\r\n              </script>\r\n              <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDW2_ChPDSYWwW5Dittons6RANEmK89QUU&amp;callback=initMap\" async defer></script>','Mapa de Google','admin','admin','2018-03-01 17:21:21','2018-08-28 17:12:59','',0),
  (10,'a2. Redes sociales','<ul class=\"social\">\r\n    <li><a class=\"icon-facebook\" href=\"REX_VALUE[1]\" target=\"_blank\"></a></li>\r\n    <li><a class=\"icon-twitter\" href=\"REX_VALUE[2]\" target=\"_blank\"></a></li>\r\n    <li><a class=\"icon-youtube\" href=\"REX_VALUE[3]\" target=\"_blank\"></a></li>\r\n</ul>','<fieldset>\r\n  <input type=\"hidden\" name=\"save\" value=\"1\">\r\n  <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n  <dl class=\"rex-form-group form-group\"><dt>\r\n    <label for=\"rex-id-meta-article-name\">Facebook link:</label>\r\n  </dt>\r\n  <dd>\r\n    <input class=\"form-control\" type=\"url\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[1]\" value=\"REX_VALUE[1]\">\r\n  </dd>\r\n  </dl>\r\n\r\n  <dl class=\"rex-form-group form-group\"><dt>\r\n    <label for=\"rex-id-meta-article-name\">Twitter link:</label>\r\n  </dt>\r\n  <dd>\r\n    <input class=\"form-control\" type=\"url\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[2]\" value=\"REX_VALUE[2]\">\r\n  </dd>\r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\"><dt>\r\n    <label for=\"rex-id-meta-article-name\">Youtube link:</label>\r\n  </dt>\r\n  <dd>\r\n    <input class=\"form-control\" type=\"url\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[3]\" value=\"REX_VALUE[3]\">\r\n  </dd>\r\n  </dl>\r\n</fieldset>','admin','admin','2018-03-02 12:12:05','2018-03-10 09:22:58','',0),
  (11,'b.Pie','<footer class=\"main-footer\">\r\n  <div class=\"rex-container\">\r\n    <div class=\"rex-item xl-35 l-40 s100\">\r\n      <h3 class=\"main-footer__title\">REX_VALUE[1]</h3>\r\n      <ul class=\"feature-list rex-container\">\r\n      <?php\r\n	if (\'REX_LINKLIST[1]\' != \'\') {\r\n		$linklist = explode(\',\',\'REX_LINKLIST[1]\');\r\n		foreach ($linklist as $link) {\r\n			$art = rex_article::get($link);\r\n			if ($art instanceof rex_article) {\r\n				$art_name = $art->getValue(\'name\');\r\n				echo \'<li class=\"rex-item col-33\"><a href=\"\'.rex_getUrl($link).\'\">\'.$art_name.\'</a></li>\';\r\n			}\r\n		}\r\n	}\r\n	?>\r\n      </ul>\r\n    </div>\r\n    <div class=\"rex-item xl-35 l-60 s100\">\r\n      <h3 class=\"main-footer__title\">REX_VALUE[2]</h3>\r\n      REX_VALUE[id=3 output=html]\r\n    </div>\r\n    <div class=\"rex-item xl-30 l-100 s100 cross-center center\">\r\n      REX_ARTICLE[17]\r\n      <p>Todos los Derechos Reservados ® <br>\r\n      <b><?php echo rex::getServerName(); ?></b>© <span id=\"currentDate\"></span></p>\r\n      <p><a class=\"ah\" href=\"//ahpublic.com\" target=\"_blank\">Diseño y Programación: AH! PUBLICIDAD</a></p>\r\n    </div>\r\n  </div>\r\n</footer>','<!-- *******************************************************\r\nFOOTER LINKS MENU\r\n******************************************************** -->\r\n<fieldset>\r\n      <dl class=\"rex-form-group form-group\"><dt>\r\n        <label for=\"rex-id-meta-article-name\">Titulo Links Menu:</label></dt><dd>\r\n        <input class=\"form-control\" type=\"text\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[1]\" value=\"REX_VALUE[1]\"></dd></dl>\r\n</fieldset>\r\n<fieldset class=\"form-horizontal\">\r\n       <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Menu Links: </label>\r\n        <div class=\"col-sm-10\">\r\n            REX_LINKLIST[id=\"1\" widget=\"1\"]\r\n        </div>\r\n    </div>\r\n</fieldset>\r\n<legend></legend>\r\n<!-- *******************************************************\r\nDIRECCIONES Y TELEFONOS\r\n******************************************************** -->\r\n<fieldset>\r\n  <dl class=\"rex-form-group form-group\"><dt>\r\n        <label for=\"rex-id-meta-article-name\">Titulo de Direcciones:</label></dt><dd>\r\n        <input class=\"form-control\" type=\"text\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[2]\" value=\"REX_VALUE[2]\"></dd></dl>\r\n</fieldset>\r\n<fieldset class=\"form-horizontal\">\r\n  <div class=\"form-group\">\r\n    <label class=\"col-sm-2 control-label\">Direcciones y telefonos:</label>\r\n    <div class=\"col-sm-10\">\r\n      <textarea class=\"redactorEditor2-basic\" name=\"REX_INPUT_VALUE[3]\">REX_VALUE[3]</textarea>\r\n    </div>\r\n  </div>\r\n</fieldset>','admin','admin','2018-03-02 15:01:35','2018-03-02 15:51:14','',0),
  (12,'a1. Inicial Links Menú','<?php\r\n	if (\'REX_LINKLIST[1]\' != \'\') {\r\n		$linklist = explode(\',\',\'REX_LINKLIST[1]\');\r\n		foreach ($linklist as $link) {\r\n			$art = rex_article::get($link);\r\n			if ($art instanceof rex_article) {\r\n                            $icon = \'\';\r\n		            $art_name = $art->getValue(\'name\');\r\n                            if ($art_name == \'Inicio\'){ $icon = \'icon-home\'; }\r\n                            if ($art_name == \'Nosotros\'){ $icon = \'icon-us\'; }\r\n                            if ($art_name == \'Contactos\'){ $icon = \'icon-contact\'; }    \r\n			    echo \'\r\n<li><a class=\"button--ghost button--small wow rotateInDownRight\" data-wow-duration=\"1s\" \r\n       data-wow-delay=\"1.2\" href=\"\'.rex_getUrl($link).\'\"><span>\'.$art_name.\'</span><i class=\"\'. $icon .\'\"></i></a></li>\';\r\n			}\r\n		}\r\n	}\r\n	?>','<!-- *******************************************************\r\n    INICIAL LINK MENU\r\n******************************************************** -->\r\n   <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Inicial Links: </label>\r\n        <div class=\"col-sm-10\">\r\n            REX_LINKLIST[id=\"1\" widget=\"1\"]\r\n        </div>\r\n    </div>','admin','admin','2018-03-10 09:21:58','2018-03-10 09:25:45','',0),
  (13,'Poppup de imagenes','          <div><img src=\"<?= rex_url::base(\'\') ?>assets/images/poppup-parker-racor.jpg\" alt=\"Parker Racor\"/>\r\n        </div>\r\n        <div><img src=\"<?= rex_url::base(\'\') ?>assets/images/poppup-parker-racor02.jpg\" alt=\"Parker racor\"/>\r\n        </div>','POPPUP GENERADO, si dea realizar cambios, contactarse con su web master','admin','admin','2018-08-09 16:00:44','2018-08-13 11:34:44','',0),
  (14,'Poppup Imágenes','<?php foreach (explode(\',\', REX_MEDIALIST[id=1]) as $image): ?>\r\n  <?php\r\n    /*Data image*/\r\n    $media = rex_media::get($image); \r\n    /*Resize imagen*/\r\n    $mediatype2 = \'multifiltros-full\';\r\n  ?>\r\n<div>\r\n  <img src=\"index.php?rex_media_type=<?= $mediatype2 ?>&rex_media_file=<?= $image ?>\" alt=\"<?php if ($media instanceof rex_media) { echo $media->getTitle();}?>\"/>\r\n</div>\r\n<?php endforeach;?>','<fieldset>\r\n            <input type=\"hidden\" name=\"save\" value=\"1\">\r\n            <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n            <dl class=\"rex-form-group form-group\"><dt><label for=\"rex-id-meta-article-name\">Insertar grupo de imágenes: </label></dt><dd>REX_MEDIALIST[id=1 widget=1]</dd></dl></fieldset>','admin','admin','2018-08-13 11:11:40','2018-08-13 11:21:44','',0),
  (15,'MARCA-LOGO','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\'); \r\n  /* Resize imagen */\r\n  $mediatype = \'multifiltros-thumb2\';\r\n  if (\"REX_MEDIA[1]\" != \'\'){\r\n    $imagen = \'index.php?rex_media_type=\' . $mediatype . \'&rex_media_file=REX_MEDIA[1]\';\r\n  }else{\r\n    $imagen = \'https://images.pexels.com/photos/265614/pexels-photo-265614.jpeg?w=1260&amp;h=750&amp;auto=compress&amp;cs=tinysrgb\';\r\n  }\r\n?>\r\n<div>\r\n  <div class=\"card card--mini\"><a class=\"card__img\" href=\"REX_VALUE[1]\" target=\"_blank\"><img src=\"<?= $imagen; ?>\" alt=\"<?php if ($media instanceof rex_media) { echo $media->getTitle();}?>\"></a>\r\n    <div class=\"card__content\">\r\n      <div class=\"card__title center\"><?php if ($media instanceof rex_media) { echo $media->getTitle();}?></div>\r\n      <footer class=\"card__footer to-center\">\r\n        <div class=\"card__button marienco-item main-center\"><a class=\"button--cta button--small\" href=\"REX_VALUE[1]\" target=\"_blank\">Visitar sitio</a></div>\r\n      </footer>\r\n    </div>\r\n  </div>\r\n</div>','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\');\r\n  ?>\r\n<fieldset>\r\n  <input type=\"hidden\" name=\"save\" value=\"1\">\r\n  <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n  <dl class=\"rex-form-group form-group\">\r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Nombre del proveedor:</label>\r\n    </dt>\r\n    <dd>\r\n      <p><?php if ($media instanceof rex_media) { echo $media->getTitle();} ?></p>\r\n    </dd>\r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Logotipo: </label>\r\n    </dt>\r\n    <dd>REX_MEDIA[id=1 widget=1]</dd>    \r\n  </dl>\r\n  <div class=\"form-group\">\r\n     <label class=\"col-sm-2 control-label\">Ver más Link: </label>\r\n     <div class=\"col-sm-10\">\r\n        <input class=\"form-control\" type=\"url\" id=\"rex-id-meta-article-name\" name=\"REX_INPUT_VALUE[1]\" value=\"REX_VALUE[1]\">\r\n     </div>\r\n  </div>\r\n</fieldset>','admin','admin','2018-08-24 12:35:09','2018-08-24 16:42:53','',0);
/*!40000 ALTER TABLE `rex_module` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_module_action`;
CREATE TABLE `rex_module_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_redactor2_profiles`;
CREATE TABLE `rex_redactor2_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `urltype` varchar(50) NOT NULL,
  `minheight` smallint(5) unsigned NOT NULL,
  `maxheight` smallint(5) unsigned NOT NULL,
  `characterlimit` smallint(5) unsigned NOT NULL,
  `toolbarfixed` tinyint(1) NOT NULL,
  `shortcuts` tinyint(1) NOT NULL,
  `linkify` tinyint(1) NOT NULL,
  `redactor_plugins` text NOT NULL,
  `redactor_customplugins` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_redactor2_profiles` WRITE;
/*!40000 ALTER TABLE `rex_redactor2_profiles` DISABLE KEYS */;
INSERT INTO `rex_redactor2_profiles` VALUES 
  (1,'full','Standard Redactor-Konfiguration','relative',300,800,0,0,0,1,'anchorlink,alignment,blockquote,bold,cleaner,clips[Snippetname1=Snippettext1|Snippetname2=Snippettext2],deleted,emaillink,externallink,fontcolor[Weiss=#ffffff|Schwarz=#000000],fontfamily[Arial|Times],fontsize[12px|15pt|120%],fullscreen,groupheading[1|2|3|4|5|6],grouplink[email|external|internal|media|telephone],grouplist[unorderedlist|orderedlist|indent|outdent],heading1,heading2,heading3,heading4,heading5,heading6,horizontalrule,internallink,italic,media,medialink,orderedlist,paragraph,properties,redo,source,styles[code=Code|kbd=Shortcut|mark=Markiert|samp=Sample|var=Variable],sub,sup,table,telephonelink,textdirection,underline,undo,unorderedlist',''),
  (2,'basic','Editor de texto basico, esencial','relative',300,800,0,0,0,0,'alignment,bold,cleaner,deleted,fullscreen,grouplink[email|external],grouplist[unorderedlist|orderedlist|indent|outdent],horizontalrule,italic,paragraph,properties,redo,source,underline,undo','');
/*!40000 ALTER TABLE `rex_redactor2_profiles` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_template`;
CREATE TABLE `rex_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `active` tinyint(1) DEFAULT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `attributes` text,
  `revision` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_template` WRITE;
/*!40000 ALTER TABLE `rex_template` DISABLE KEYS */;
INSERT INTO `rex_template` VALUES 
  (1,'01.Inicio','REX_TEMPLATE[2] <?php //HEAD y HEADER?>\r\nREX_TEMPLATE[8] <?php //MENU-MOVIL?>\r\nREX_TEMPLATE[4] <?php //MENU-ASIDE?>\r\n<main class=\"rex-item xl-85 l-80\">\r\n<div class=\"main-main\">\r\n  <transition name=\"modal--slider\">\r\n  <template v-if=\"showmodal\">\r\n    <div :class=\"{ \'modal-popup\': showmodal }\">\r\n      <div class=\"modal-popup__close\" v-on:click=\"showmodal=false\"><b>Cerrar</b><svg width=\"30\" height=\"30\"><g stroke=\"rgb(160,160,160)\" stroke-width=\"4\"><line x1=\"5\" y1=\"5\" x2=\"25\" y2=\"25\"></line><line x1=\"5\" y1=\"25\" x2=\"25\" y2=\"5\"></line></g></svg></div>\r\n      <div class=\"modal-popup__background\" v-on:click=\"showmodal=false\"></div>\r\n      <div class=\"my-slider\">\r\n        REX_ARTICLE[21] <?php //POPPUP DE IMAGENES ?>  \r\n      </div>\r\n    </div>\r\n  </template>\r\n</transition>\r\n  <div class=\"banner-slides\">\r\n   <div class=\"rex-slider wow fadeInDown\" data-wow-duration=\"2s\" data-wow-delay=\"0s\">    \r\n    REX_ARTICLE[14] <?php //SLIDER DE IMAGENES ?>\r\n   </div>\r\n  </div>\r\n  <div class=\"rex-item\">\r\n    <h2 class=\"center\">MultiFiltros Santa Cruz S.R.L.</h2>\r\n    <div class=\"main-main__content l-90 s-100 to-center l-block\">\r\n    </div>\r\n    REX_ARTICLE[]\r\n  </div>\r\n</div>\r\n</main>\r\nREX_TEMPLATE[3] <?php //FOOTER y SCRIPTS?>',1,'admin','admin','2018-08-24 14:58:36','2018-08-24 14:58:36','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0),
  (2,'head','<!DOCTYPE html>\r\n<html lang=\"<?php echo rex_clang::getCurrent()->getCode(); ?>\" prefix=\"og: http://ogp.me/ns#\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"initial-scale=1.0, width=device-width\">\r\n    <meta name=\"designer\" content=\"Fernando Javier Averanga Aruquipa / nandes.ingsistemas@gmail.com\">\r\n    <?php\r\n	// Use article title as title-Tag, unless a custom title-tag is set\r\n    if ($this->hasValue(\"art_title\") && $this->getValue(\"art_title\") != \"\") {\r\n		$title = htmlspecialchars($this->getValue(\'art_title\'));\r\n	} else {\r\n		$title = htmlspecialchars($this->getValue(\'name\'));\r\n	}\r\n    if($title != \'Inicio\'){\r\n	  echo \'<title>\'.$title.\' | \' . rex::getServerName() . \' - Venta, distribución de filtros y lubricantes para vehiculos</title>\';\r\n    }else{\r\n      echo \'<title>\' . rex::getServerName() . \' - Venta, distribución de filtros y lubricantes para vehiculos</title>\';\r\n    }\r\n\r\n        echo \'<meta property=\"og:title\" content=\"\'.$title.\' | \' . rex::getServerName() . \' - Venta, distribución de filtros y lubricantes para vehiculos\">\';\r\n\r\n	// Keywords and description\r\n	// If current article does not have keywords and description, take them from start article\r\n	$keywords = \"\";\r\n    if ($this->hasValue(\"art_keywords\") && $this->getValue(\"art_keywords\") != \"\") {\r\n        $keywords = $this->getValue(\"art_keywords\");\r\n    } else {\r\n        $home = new rex_article_content(rex_article::getSiteStartArticleId());\r\n        if ($home->hasValue(\"art_keywords\")) {\r\n            $keywords = $home->getValue(\'art_keywords\');\r\n        }\r\n    }\r\n\r\n    $description = \"Filtros para autos y vehiculos de las marcas Tecfil, Unifilter, Racor, Keltec, Baldwin, Rama. Lubricantes, refrigerantes, siliconas, bombas de lubricación. Procesador de conbustible especial para la inyección electrónica.\";\r\n    if ($this->hasValue(\"art_description\") && $this->getValue(\"art_description\") != \"\") {\r\n        $description = $this->getValue(\"art_description\");\r\n    } else {\r\n        $home = new rex_article_content(rex_article::getSiteStartArticleId());\r\n        if ($home->hasValue(\"art_description\")) {\r\n            $description = $home->getValue(\'art_description\');\r\n        }\r\n    }\r\n\r\n	echo \'\r\n	<meta name=\"keywords\" content=\"\'.htmlspecialchars($keywords).\'\">\';\r\n\r\n	echo \'\r\n	<meta name=\"description\" content=\"\'.htmlspecialchars($description).\'\">\';\r\n\r\n        echo \'\r\n	<meta property=\"og:description\" content=\"\'.htmlspecialchars($description).\'\">\';\r\n	?>\r\n<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Lato:400,400i,700|Open+Sans:700\">\r\n    <link rel=\"stylesheet\" href=\"<?= rex_url::base(\'assets/css/styles.css?v=1.6.0\') ?>\">\r\n  </head>\r\n  <body>\r\n    <div id=\"multifiltros\" class=\"poppup_slider\">\r\n      REX_ARTICLE[16]\r\n      <div class=\"rex-container main-section l-section\">\r\n\r\n<div class=\"movil-menu\" :class=\"toggle ? \'show\' : \'\'\">\r\n          <nav class=\"movil-menu__inicial\">\r\n            <ul>\r\n              <li v-for=\"item in menuinicio\"><a class=\"button--ghost button--small wow rotateInDownRight\" data-wow-duration=\"1s\" :data-wow-delay=\"item.wow_delay\" :href=\"path_page + item.href\"><span v-text=\"item.title\"></span><i :class=\"item.icon\"></i></a></li>\r\n            </ul>\r\n          </nav>\r\n          <nav class=\"vertical-menu movil-menu__content\">\r\n            <ul>\r\n              <li v-for=\"item in mainmenu\"><a :href=\"path_page + item.href\"><span v-text=\"item.title\"></span></a></li>\r\n            </ul>\r\n          </nav>\r\n        </div>',0,'soporte-multifiltros','soporte-multifiltros','2018-08-24 18:13:05','2018-08-24 18:13:05','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0),
  (3,'footer','</div>\r\nREX_TEMPLATE[9] <?php //CAROUSEL PRE-FOOTER ?>\r\nREX_ARTICLE[15]\r\n<a class=\"button--goup fade-out button--cta\" id=\"goup\" href=\"#\"><i class=\"icon-up\"></i></a>\r\n    </div>\r\n    <script src=\"<?= rex_url::base(\'assets/js/modernizr.js\') ?>\"></script>\r\n    <script src=\"<?= rex_url::base(\'assets/js/script.js?v=1.6.0\') ?>\"></script>\r\n  </body>\r\n</html>',0,'soporte-multifiltros','soporte-multifiltros','2018-08-25 12:41:39','2018-08-25 12:41:39','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0),
  (4,'menu-aside','<aside class=\"rex-item xl-15 l-20 main-aside sidebar-first\" data-sticky-container>\r\n        <nav class=\"vertical-menu\" id=\"vertical-menu\">\r\n          <div class=\"sticky-menu\">\r\n            <ul>\r\n              REX_ARTICLE[13]\r\n            </ul>\r\n          </div>\r\n        </nav>\r\n      </aside>',0,'admin','admin','2018-03-09 17:56:25','2018-03-09 17:56:25','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0),
  (6,'02.Plantilla','REX_TEMPLATE[2] <?php //HEAD y HEADER?>\r\nREX_TEMPLATE[8] <?php //MENU-MOVIL?>\r\nREX_TEMPLATE[4] <?php //MENU-ASIDE?>\r\n<main class=\"rex-item xl-85 l-80\">\r\n<div class=\"main-main\">\r\n  <div class=\"rex-item rex-container\">    \r\n    REX_ARTICLE[]\r\n  </div>\r\n</div>\r\n</main>\r\nREX_TEMPLATE[3] <?php //FOOTER y SCRIPTS?>',1,'admin','admin','2018-03-20 10:54:52','2018-03-20 10:54:52','{\"ctype\":[],\"modules\":{\"1\":{\"0\":\"4\",\"1\":\"2\",\"2\":\"5\",\"3\":\"6\",\"4\":\"9\",\"5\":\"3\",\"all\":0}},\"categories\":{\"all\":\"1\"}}',0),
  (7,'03.Complementos','REX_TEMPLATE[2] <?php //HEAD y HEADER?>\r\nREX_TEMPLATE[4] <?php //MENU-ASIDE?>\r\n<main class=\"rex-item xl-85 l-80\">\r\n<div class=\"main-main rex-container\">\r\n  <div class=\"rex-container rex-item\">\r\n    <div class=\"rex-item main-banner--dark main-banner\"><div class=\"rex-container\">\r\n      <div class=\"rex-item l-75 to-center\">\r\n          <img src=\"https://images.pexels.com/photos/265614/pexels-photo-265614.jpeg?w=1260&amp;h=750&amp;auto=compress&amp;cs=tinysrgb\" alt=\"Rama\" class=\"main-banner__img\"> \r\n        <div class=\"main-banner__data\">\r\n             <h1>Esta pagina es inaccesible</h1>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"rex-item\">\r\n    <div class=\"l-90 s-100 to-center l-block\">\r\n        <p>Haga click en el boton Ir a pagina principal.</p>\r\n        <a href=\"<?= rex_url::base(\'\') ?>\" class=\"button\">Ir a pagina principal</a> \r\n     </div>\r\n  </div>\r\n</div>\r\n</main>\r\nREX_TEMPLATE[3] <?php //FOOTER y SCRIPTS?>',1,'admin','admin','2018-08-24 12:42:24','2018-08-24 12:42:24','{\"ctype\":[],\"modules\":{\"1\":{\"0\":\"4\",\"1\":\"7\",\"2\":\"12\",\"3\":\"10\",\"4\":\"11\",\"5\":\"15\",\"6\":\"13\",\"7\":\"14\",\"8\":\"8\",\"9\":\"1\",\"all\":0}},\"categories\":{\"all\":\"1\"}}',0),
  (8,'menu-movil','<div class=\"movil-menu\" :class=\"toggle ? \'show\' : \'\'\">\r\n          <nav class=\"movil-menu__inicial\">\r\n            <ul>\r\n              REX_ARTICLE[18]\r\n            </ul>\r\n          </nav>\r\n          <nav class=\"vertical-menu movil-menu__content\">\r\n            <ul>\r\n              REX_ARTICLE[13]\r\n            </ul>\r\n          </nav>\r\n        </div>',0,'admin','admin','2018-03-10 09:38:12','2018-03-10 09:38:12','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0),
  (9,'Carousel de marcas','<div class=\"carousel l-block\">\r\n  <div class=\"main-main\">\r\n    <h2 class=\"l-block t1\">NUESTRAS MARCAS: </h2>\r\n    <div class=\"carousel-slider\">\r\n     REX_ARTICLE[22]\r\n    </div>\r\n  </div>\r\n</div>',0,'admin','admin','2018-08-24 15:33:28','2018-08-24 15:33:28','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0);
/*!40000 ALTER TABLE `rex_template` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_user_role`;
CREATE TABLE `rex_user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `perms` text NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_user_role` WRITE;
/*!40000 ALTER TABLE `rex_user_role` DISABLE KEYS */;
INSERT INTO `rex_user_role` VALUES 
  (1,'Administrador','Usuario Administrador','{\"general\":\"|users[]|backup[export]|media_manager[]|multiupload[]|\",\"options\":\"|moveSlice[]|publishCategory[]|copyContent[]|article2startarticle[]|article2category[]|copyArticle[]|moveArticle[]|moveCategory[]|publishArticle[]|\",\"extras\":null,\"clang\":\"all\",\"media\":\"all\",\"structure\":\"all\",\"modules\":\"all\",\"yform_manager_table\":\"all\"}','admin','admin','2018-03-19 12:43:17','2018-03-19 16:29:44',0);
/*!40000 ALTER TABLE `rex_user_role` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_yform_email_template`;
CREATE TABLE `rex_yform_email_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `mail_from` varchar(255) NOT NULL,
  `mail_from_name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `body_html` text NOT NULL,
  `attachments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yform_field`;
CREATE TABLE `rex_yform_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL,
  `prio` int(11) NOT NULL,
  `type_id` varchar(100) NOT NULL,
  `type_name` varchar(100) NOT NULL,
  `list_hidden` tinyint(1) NOT NULL,
  `search` tinyint(1) NOT NULL,
  `name` text NOT NULL,
  `label` text NOT NULL,
  `not_required` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yform_history`;
CREATE TABLE `rex_yform_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL,
  `dataset_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dataset` (`table_name`,`dataset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yform_history_field`;
CREATE TABLE `rex_yform_history_field` (
  `history_id` int(11) NOT NULL,
  `field` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`history_id`,`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yform_table`;
CREATE TABLE `rex_yform_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `list_amount` tinyint(3) unsigned NOT NULL DEFAULT '50',
  `list_sortfield` varchar(255) NOT NULL DEFAULT 'id',
  `list_sortorder` enum('ASC','DESC') NOT NULL DEFAULT 'ASC',
  `prio` int(11) NOT NULL,
  `search` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `export` tinyint(1) NOT NULL,
  `import` tinyint(1) NOT NULL,
  `mass_deletion` tinyint(1) NOT NULL,
  `mass_edit` tinyint(1) NOT NULL,
  `history` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_name` (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yrewrite_alias`;
CREATE TABLE `rex_yrewrite_alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias_domain` varchar(255) NOT NULL,
  `domain_id` int(11) NOT NULL,
  `clang_start` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yrewrite_domain`;
CREATE TABLE `rex_yrewrite_domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  `mount_id` int(11) NOT NULL,
  `start_id` int(11) NOT NULL,
  `notfound_id` int(11) NOT NULL,
  `clangs` varchar(255) NOT NULL,
  `clang_start` int(11) NOT NULL,
  `clang_start_hidden` tinyint(1) NOT NULL,
  `robots` text NOT NULL,
  `title_scheme` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yrewrite_forward`;
CREATE TABLE `rex_yrewrite_forward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `article_id` int(11) NOT NULL,
  `clang` int(11) NOT NULL,
  `extern` varchar(255) NOT NULL,
  `media` varchar(255) NOT NULL,
  `movetype` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS = 1;
