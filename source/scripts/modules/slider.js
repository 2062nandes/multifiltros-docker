export const options_slider = {
  mode: 'gallery',
  container: '.rex-slider',
  controlsText: [
    '<i class="icon-left"></i>',
    '<i class="icon-right"></i>'
  ],
  autoplayText: [
    '<i class="icon-play"></i>',
    '<i class="icon-pause"></i>'
  ],
  animateIn: 'bounceInDown' ,
  animateOut: 'bounceOutUp',
  speed: 2000,
  autoplay: true
};