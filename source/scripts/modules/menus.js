export const mainmenu = [
  {
    title: 'Tecfil',
    href: 'tecfil.html',
    description: 'Descripción de prueba',
    image: 'images/1.jpg',
    class: ''
  },
  {
    title: 'UniFilter',
    href: 'unifilter.html',
    description: 'Descripción de prueba',
    image: 'images/2.jpg',
    class: ''
  },
  {
    title: 'Racor',
    href: 'racor.html',
    description: 'Descripción de prueba',
    image: 'images/3.jpg',
    class: ''
  },
  {
    title: 'Keltec',
    href: 'keltec.html',
    description: 'Descripción de prueba',
    image: 'images/4.jpg',
    class: ''
  },
  {
    title: 'Baldwin',
    href: 'baldwin.html',
    description: 'Descripción de prueba',
    image: 'images/5.jpg',
    class: ''
  },
  {
    title: 'Mc repuestos',
    href: 'mc-repuestos.html',
    description: 'Descripción de prueba',
    image: 'images/1.jpg',
    class: ''
  },
  {
    title: 'Rama',
    href: 'rama.html',
    description: 'Descripción de prueba',
    image: 'images/2.jpg',
    class: ''
  },
  {
    title: 'Lubricantes',
    href: 'lubricantes.html',
    description: 'Descripción de prueba',
    image: 'images/3.jpg',
    class: ''
  },
  {
    title: 'Accesorios',
    href: 'accesorios.html',
    description: 'Descripción de prueba',
    image: 'images/4.jpg',
    class: ''
  },
];

export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home',
    wow_delay: '0.3s'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-us',
    wow_delay: '0.5s'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact',
    wow_delay: '0.7s'
  }
];
