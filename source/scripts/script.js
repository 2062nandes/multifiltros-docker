// Via CommonJS
  // Sticky-js
var Sticky = require('sticky-js/dist/sticky.min');
// Via AMD
  import { menu } from "./modules/menu";
  // import { edTabs } from "./modules/tabs";

  // Slider VanillaJS (https://github.com/ganlanyuan/tiny-slider)
  import { tns } from "../../node_modules/tiny-slider/src/tiny-slider.module";
  import { options_slider } from "./modules/slider";
  
  // Imagenes desplegables baguetteBox.js
  // import BaguetteBox from 'baguettebox.js/dist/baguetteBox.min.js';
//    ../../node_modules/
  //WOW JS
  import WOW from 'wow.js/dist/wow.min'
  //Data VueJS
  import { contactform } from './modules/contactform'
  import { menuinicio, mainmenu } from './modules/menus'

  import Vue from 'vue/dist/vue.min'
  // import Vue from 'vue/dist/vue'
  import VueResource from 'vue-resource/dist/vue-resource.min'
  Vue.use(VueResource);

const vm = new Vue({
  el: '#multifiltros',
  data: {
    showmodal     : true,
    path_page     : '/demo/',
    path_media    : '/assets/',
    toggle        : false,
    formSubmitted : false,
    vue           : contactform,
    menuinicio,
    mainmenu
  },
  created: function () {
    const wow = new WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: false,
      live: false
    })
    wow.init()
    // window.edTabs = edTabs;
    
    //FOOTER AÑO
    function getDate() {
      var today = new Date()
      var year = today.getFullYear()
      document.getElementById('currentDate').innerHTML = year
    }
    getDate()
  },
  mounted: function () {    
    var sticky = new Sticky('.sticky-menu');
    //Menu Vertical Active
    const activeMenuItem = containerId => {
      let links = [...document.querySelectorAll(`#${containerId} a`)];
      const curentUrl = document.location.href;
      links.map(link => {
        if (link.href === curentUrl) {
          link.classList.add('active')
        }
      });
    };
    activeMenuItem('vertical-menu');
    // Tiny Slider Active
    const slider = tns(options_slider);
    
    const poppup = tns({
      container: '.my-slider',
      controlsText: [
        '<',
        '>'
      ],
      // autoplayText: ['▶', '❚❚'],
      autoplay: true,
      // autoplayDirection: 'backward',
      speed: 2000,
      autoplayTimeout: 7000,
      lazyload: true,
      // animateIn: 'flipInY',
      // animateOut: 'bounceOut'
    });

    /* CAROSUEL PRE-FOOTER */
    const carousel = tns({
      container: '.carousel-slider',
      speed: 1200,
      autoplay: true,
      autoplayTimeout: 2500,
      // autoplayHoverPause: true,
      responsive: {
        "0": {
          items: 2
        },
        "640": {
          items: 3
        },
        "950": {
          items: 4
        },
        "1124": {
          items: 5
        },
        "1240": {
          items: 6
        },
        "1400": {
          items: 7
        }
      },
    });

    // BaguetteBox.run('.card__content', {
    //   // Custom options
    // });

    // Button ir arriba
    let scrollV = window.scrollY;    
    const goup = document.getElementById("goup");
    const goup_event = window.addEventListener('scroll', function () {
      scrollV = window.scrollY;
      if (scrollV > 120) {
        goup.classList.remove('fade-out');
        goup.classList.add('fade-in');
      }
      else {
        goup.classList.remove('fade-in');
        goup.classList.add('fade-out');
      }
      // console.log(scrollV);
     });
  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('../../mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    menuToggle: function () {
      if (this.toggle == true) {
        this.toggle = false
        // console.log('DESACTIVADO')
      }
      else {
        this.toggle = true
        // console.log('ACTIVO')
      }
    }
  }
})